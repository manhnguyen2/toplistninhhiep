@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{env('APP_NAME')}}"/>
<meta name="description" content="{{env('APP_NAME')}}"/>
<meta property="og:image" content="{{env('APP_LOGO')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{env('APP_NAME')}}">
<meta property="og:description" content="{{env('APP_NAME')}}">
@stop
@section('styles')
<link rel="stylesheet" href="/themes/townhub/css/home.css">
@endsection
@section('content')
<section   class="gray-bg hidden-section particles-wrapper">
    <div class="container">
        <div class="section-title">
            <h2>Chào mừng đến với TopList Ninh Hiệp</h2>
            <div class="section-subtitle">Chào mừng đến với TopList Ninh Hiệp</div>
            <span class="section-separator"></span>
            <p>Tìm kiếm Đối tác, Cửa hàng, Nguồn hàng,... tại Ninh Hiệp Nhanh chóng & Miễn phí 💚</p>
        </div>
        <div class="listing-item-grid_container fl-wrap">
            <div class="row">
                @foreach(\App\Models\Category::whereNull('parent_id')->get() as $item)
                <div class="col-sm-4">
                    <div class="listing-item-grid">
                        <div class="bg"  data-bg="{{$item->image}}"></div>
                        <div class="d-gr-sec"></div>
                        <div class="listing-item-grid_title">
                            <h3><a href="{{$item->url}}">{{$item->title}}</a></h3>
                            <p>{{$item->description}}</p>
                        </div>
                    </div>
                </div>
                @endforeach                                                                                     
            </div>
        </div>
    </div>
</section>
<section class="slw-sec" id="sec1">
    <div class="section-title">
        <h2>Danh mục vị trí</h2>
        <div class="section-subtitle">Danh mục vị trí</div>
        <span class="section-separator"></span>
        <p>Phân loại danh sách theo địa chỉ đường xá, thôn xóm tại Ninh Hiệp.</p>
    </div>
    <div class="listing-slider-wrap fl-wrap">
        <div class="listing-slider fl-wrap">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach($positions as $item)
                    <div class="swiper-slide">
                        <div class="listing-slider-item fl-wrap">
                            <div class="listing-item listing_carditem">
                                <article class="geodir-category-listing fl-wrap">
                                    <div class="geodir-category-img">
                                        <a href="{{$item->url}}" class="geodir-category-img-wrap fl-wrap">
                                            <img style="height: 300px; object-fit: cover;" src="{{ $item->image }}" alt="{{ $item->title }}"> 
                                        </a>
                                        <div class="geodir-category-opt">
                                            <div class="geodir-category-opt_title">
                                                <h4><a href="{{$item->url}}">{{ $item->title }}</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    @endforeach                                   
                </div>
            </div>
            <div class="listing-carousel-button listing-carousel-button-next2"><i class="fas fa-caret-right"></i></div>
            <div class="listing-carousel-button listing-carousel-button-prev2"><i class="fas fa-caret-left"></i></div>
        </div>
        <div class="tc-pagination_wrap">
            <div class="tc-pagination2"></div>
        </div>
    </div>
</section>
@if(!auth()->check() || (auth()->check() && !auth()->user()->store))
<section class="gradient-bg hidden-section" data-scrollax-parent="true">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="colomn-text  pad-top-column-text fl-wrap">
                    <div class="colomn-text-title">
                        <h3>ĐĂNG KÝ NGAY !</h3>
                        <p>Đưa thương hiệu của bạn lên nền tảng trực tuyến. Tiếp cận với hàng nghìn khách hàng tiềm năng mỗi ngày!</p>
                        <a href="{{route('register')}}" class="down-btn color3-bg"><i class="fab fa-apple"></i>  Đăng ký thành viên </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div><img src="/userfiles/files/5e016be73a46f718ae57.jpg" height="350"></div>
            </div>
        </div>
    </div>
    <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
    <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
    <div class="circle-wrap" style="left:270px;top:120px;" data-scrollax="properties: { translateY: '-200px' }">
        <div class="circle_bg-bal circle_bg-bal_small"></div>
    </div>
    <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
        <div class="circle_bg-bal circle_bg-bal_big"></div>
    </div>
    <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
        <div class="circle_bg-bal circle_bg-bal_big"></div>
    </div>
    <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
        <div class="circle_bg-bal circle_bg-bal_middle"></div>
    </div>
    <div class="circle-wrap" style="right:40%;top:-10px;"  >
        <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
    </div>
    <div class="circle-wrap" style="right:55%;top:90px;"  >
        <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
    </div>
</section>
@endif
@foreach($stores as $key=>$item)
<section class="{{true?'':'gray-bg'}}">
    <div class="container big-container">
        <div class="section-title">
            <h2><span>{{$key}}</span></h2>
            <div class="section-subtitle">{{$key}}</div>
            <span class="section-separator"></span>
            <p>Danh sách các cửa hàng vải, cửa hàng phụ kiện</p>
        </div>
        <div class="grid-item-holder gallery-items fl-wrap">
            @include('home.__store',['stores'=>$item])
        </div>
        <a href="{{route('category.index',\Illuminate\Support\Str::slug(strtolower($key)))}}" class="btn  dec_btn  color2-bg">Xem tất cả<i class="fal fa-arrow-alt-right"></i></a>
    </div>
</section>
@endforeach
<div class="sec-circle fl-wrap"></div>
<section class="slw-sec">
    <div class="section-title">
        <h2>Tin tức</h2>
        <div class="section-subtitle">Tin tức</div>
        <span class="section-separator"></span>
        <p>Cập nhật tin tức mới nhất</p>
    </div>
    <div class="listing-slider-wrap fl-wrap">
        <div class="listing-slider fl-wrap">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach(\App\Models\Post::latest()->get() as $item)
                    <div class="swiper-slide">
                        <div class="listing-slider-item fl-wrap">
                            <div class="listing-item listing_carditem">
                                <article class="geodir-category-listing fl-wrap">
                                    <div class="geodir-category-img">
                                        <a href="{{$item->url}}" class="geodir-category-img-wrap fl-wrap">
                                            <img src="{{$item->image}}" alt=""> 
                                        </a>
                                        <div class="geodir-category-opt">
                                            <div class="geodir-category-opt_title">
                                                <h4><a href="{{$item->url}}">{{$item->title}}</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    @endforeach                                   
                </div>
            </div>
            <div class="listing-carousel-button listing-carousel-button-next2"><i class="fas fa-caret-right"></i></div>
            <div class="listing-carousel-button listing-carousel-button-prev2"><i class="fas fa-caret-left"></i></div>
        </div>
        <div class="tc-pagination_wrap">
            <div class="tc-pagination2"></div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Home ready')
    })
</script>
@endsection