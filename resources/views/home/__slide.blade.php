<!--Hero slider-->
<div class="hero-slider_wrap fl-wrap">
    <div class="hero-slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <!--hero-slider-item-->
                <div class="swiper-slide">
                    <div class="hero-slider-item fl-wrap">
                        <div class="bg-tabs-wrap">
                            <div class="bg"  data-bg="/themes/townhub/images/bg/37.jpg"></div>
                            <div class="overlay op7"></div>
                        </div>
                        <div class="container small-container">
                            <div class="intro-item fl-wrap">
                                <span class="section-separator"></span>
                                <div class="bubbles">
                                    <h1>Chào mừng đến với Ninh Hiệp Online</h1>
                                </div>
                                <h3>Tìm kiếm Đối tác, Cửa hàng, Nguồn hàng,... tại Ninh Hiệp Nhanh chóng & Miễn phí 💚</h3>
                            </div>
                            <div class="hero-search  fl-wrap">
                                <div class="main-search-input-wrap fl-wrap">
                                    <div class="main-search-input fl-wrap">
                                        <div class="main-search-input-item">
                                            <label><i class="fal fa-keyboard"></i></label>
                                            <input type="text" placeholder="What are you looking for?" value=""/>
                                        </div>
                                        <div class="main-search-input-item location autocomplete-container">
                                            <label><i class="fal fa-map-marker-check"></i></label>
                                            <input type="text" placeholder="Location" class="autocomplete-input" id="autocompleteid" value=""/>
                                            <a href="#"><i class="fa fa-dot-circle-o"></i></a>
                                        </div>
                                        <div class="main-search-input-item">
                                            <select data-placeholder="All Categories"  class="chosen-select no-search-select" >
                                                <option>All Categories</option>
                                                <option>Shops</option>
                                                <option>Hotels</option>
                                                <option>Restaurants</option>
                                                <option>Fitness</option>
                                                <option>Events</option>
                                            </select>
                                        </div>
                                        <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--hero-slider-item end-->
                <!--hero-slider-item-->
                <div class="swiper-slide">
                    <div class="hero-slider-item fl-wrap">
                        <div class="bg-tabs-wrap">
                            <div class="bg"  data-bg="/themes/townhub/images/bg/7.jpg"></div>
                            <div class="overlay op7"></div>
                        </div>
                        <div class="container small-container">
                            <div class="intro-item fl-wrap">
                                <span class="section-separator"></span>
                                <div class="bubbles">
                                    <h1>Find Best Restaurants and Cafe</h1>
                                </div>
                                <h3>Find some of the best tips from around the city from our partners and friends.</h3>
                            </div>
                            <div class="hero-search  fl-wrap">
                                <div class="main-search-input-wrap fl-wrap">
                                    <div class="main-search-input fl-wrap">
                                        <div class="main-search-input-item">
                                            <label><i class="fal fa-cheeseburger"></i></label>
                                            <input type="text" placeholder="Restaurant Name" value=""/>
                                        </div>
                                        <div class="main-search-input-item clact date-container4">
                                            <span class="iconn-dec"><i class="fal fa-calendar"></i></span>
                                            <input type="text" placeholder="Date and Time"     name="datepicker-here-time"   value=""/>
                                            <span class="clear-singleinput"><i class="fal fa-times"></i></span>
                                        </div>
                                        <div class="main-search-input-item">
                                            <label><i class="fal fa-user-friends"></i></label>
                                            <input type="number" placeholder="Persons" value=""/>
                                        </div>
                                        <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--hero-slider-item end-->                                    
                <!--hero-slider-item-->
                <div class="swiper-slide">
                    <div class="hero-slider-item fl-wrap">
                        <div class="bg-tabs-wrap">
                            <div class="bg"  data-bg="/themes/townhub/images/bg/5.jpg"></div>
                            <div class="overlay op7"></div>
                        </div>
                        <div class="container small-container">
                            <div class="intro-item fl-wrap">
                                <span class="section-separator"></span>
                                <div class="bubbles">
                                    <h1>Visit Events and  Clubs</h1>
                                </div>
                                <h3>Find some of the best tips from around the city from our partners and friends.</h3>
                            </div>
                            <div class="hero-search  fl-wrap">
                                <div class="main-search-input-wrap fl-wrap">
                                    <div class="main-search-input fl-wrap">
                                        <div class="main-search-input-item">
                                            <label><i class="fal fa-handshake-alt"></i></label>
                                            <input type="text" placeholder="Event Name or Place" value=""/>
                                        </div>
                                        <div class="main-search-input-item">
                                            <select data-placeholder="Loaction" class="chosen-select on-radius no-search-select" >
                                                <option>All Cities</option>
                                                <option>New York</option>
                                                <option>London</option>
                                                <option>Paris</option>
                                                <option>Kiev</option>
                                                <option>Moscow</option>
                                                <option>Dubai</option>
                                                <option>Rome</option>
                                                <option>Beijing</option>
                                            </select>
                                        </div>
                                        <div class="main-search-input-item clact date-container4">
                                            <span class="iconn-dec"><i class="fal fa-calendar"></i></span>
                                            <input type="text" placeholder="Event Date"     name="datepicker-here"   value=""/>
                                            <span class="clear-singleinput"><i class="fal fa-times"></i></span>
                                        </div>
                                        <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--hero-slider-item end-->                                    
            </div>
        </div>
    </div>
    <div class="listing-carousel_pagination hero_pagination">
        <div class="listing-carousel_pagination-wrap"></div>
    </div>
    <div class="slider-hero-button-prev shb color2-bg"><i class="fas fa-caret-left"></i></div>
    <div class="slider-hero-button-next shb color2-bg"><i class="fas fa-caret-right"></i></div>
</div>
<!--Hero slider end-->