@foreach($stores as $store)
<div class="gallery-item restaurant events">
    <div class="listing-item">
        <article class="geodir-category-listing fl-wrap">
            <div class="geodir-category-img">
                <a href="{{$store->url}}" class="geodir-category-img-wrap fl-wrap">
                    <img style="height: 200px;" src="{{collect($store->banner)->first()?:'/themes/townhub/images/all/1.jpg'}}" alt="{{$store->title}}"> 
                </a>
                <div class="listing-avatar"><a href="{{$store->url}}"><img src="{{$store->avatar?:'/themes/townhub/images/avatar/1.jpg'}}" alt="{{$store->title}}"></a>
                    <span class="avatar-tooltip">Added By  <strong>{{$store->user->name??'anonymous'}}</strong></span>
                </div>
                <div class="geodir-category-opt">
                    <div class="listing-rating-count-wrap">
                        <div class="review-score">4.8</div>
                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                        <br>
                        <div class="reviews-count">12 reviews</div>
                    </div>
                </div>
            </div>
            <div class="geodir-category-content fl-wrap title-sin_item">
                <div class="geodir-category-content-title fl-wrap">
                    <div class="geodir-category-content-title-item">
                        <h3 class="title-sin_map"><a href="{{$store->url}}">{{ $store->title }}</a><span class="verified-badge"><i class="fal fa-check"></i></span></h3>
                        <div class="geodir-category-location fl-wrap"><a href="javascript:;" ><i class="fas fa-map-marker-alt"></i> {{$store->address?:'-'}}</a></div>
                    </div>
                </div>
                <div class="geodir-category-text fl-wrap">
                    <p class="small-text">{{$store->description?:'-'}}</p>
                </div>
                <div class="geodir-category-footer fl-wrap">
                    <a class="listing-item-category-wrap">
                        <div class="listing-item-category red-bg"><img  src="{{ asset($store->categories->first()->image??'') }}" alt="{{ $store->categories->first()->title??'' }}"></div>
                        <span>{{ $store->categories->first()->title??'' }}</span>
                    </a>
                    <div class="geodir-opt-list">
                        <ul class="no-list-style">
                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">Contact Info</span></a></li>
                            <li><a href="#1" class="single-map-item" data-newlatitude="40.72956781" data-newlongitude="-73.99726866"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map <strong>1</strong></span> </a></li>
                            <li>
                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/1.jpg'},{'src': 'images/all/24.jpg'}, {'src': 'images/all/12.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip">Gallery</span></div>
                            </li>
                        </ul>
                    </div>
                    <div class="geodir-category_contacts">
                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                        <ul class="no-list-style">
                            <li><span><i class="fal fa-phone"></i> Call : </span><a href="tel:{{str_replace(' ', '', $store->phone)}}">{{$store->phone}}</a></li>
                            <li><span><i class="fal fa-envelope"></i> Write : </span><a href="mailto:{{$store->user->email}}">{{$store->user->email}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
@endforeach 