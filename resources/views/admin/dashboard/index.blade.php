@extends('layouts.admin')

@section('content')

<!-- Content area -->
<div class="content">
 <!-- Quick stats boxes -->
 <div class="row">
            <div class="col-lg-4">

                <!-- Members online -->
                <div class="card bg-teal-400">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $count_store }}</h3>
                        </div>
                        
                        <div>
                            Gian hàng
                            <div class="font-size-sm opacity-75">Số lượng gian hàng đăng ký trên hệ thống</div>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div id="members-online"></div>
                    </div>
                </div>
                <!-- /members online -->

            </div>

            <div class="col-lg-4">

                <!-- Current server load -->
                <div class="card bg-pink-400">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $count_user }}</h3>
                            <div class="list-icons ml-auto">
                                <div class="list-icons ml-auto">
                                    <a class="list-icons-item" href="{{ route('admin.store.show') }}"></a>
                                </div>
                            </div>
                        </div>
                        
                        <div>
                            Thành viên
                            <div class="font-size-sm opacity-75">Số lượng thành viên hoạt động trên hệ thống</div>
                        </div>
                    </div>

                    <div id="server-load"></div>
                </div>
                <!-- /current server load -->

            </div>

            <div class="col-lg-4">

                <!-- Today's revenue -->
                <div class="card bg-blue-400">
                    <div class="card-body">
                        <div class="d-flex">
                            <h3 class="font-weight-semibold mb-0">{{ $count_post }}</h3>
                        </div>
                        
                        <div>
                            Bài viết
                            <div class="font-size-sm opacity-75">Tổng số bài viết trên hệ thống</div>
                        </div>
                    </div>
                </div>
                <!-- /today's revenue -->

            </div>
        </div>
        <!-- /quick stats boxes -->
<!-- Dashboard content -->
<div class="row">
    <div class="col-xl-8">

        <!-- Marketing campaigns -->
        <div class="card">
            <div class="card-header header-elements-sm-inline">
                <h6 class="card-title">Cửa hàng đăng ký gần đây</h6>
                <div class="header-elements">
                    <span class="badge bg-success badge-pill">28 hoạt động</span>
                </div>
            </div>

            <div class="card-body d-sm-flex align-items-sm-center justify-content-sm-between flex-sm-wrap">
                <div>
                    <a href="{{ route('admin.store.show') }}" class="btn bg-indigo-300"><i class="icon-statistics mr-2"></i> Xem tất cả</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table text-nowrap" id="dataTable">
                    <thead>
                        <tr>
                            <th>Tên cửa hàng</th>
                            <th>Tên khách hàng</th>
                            <th>Loại cửa hàng</th>
                            <th>Danh mục</th>
                            <th>Trạng thái</th>
                            <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($store as $item)
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="mr-3">
                                        <a href="#">
                                            <img src="{{ asset($item->avatar) }}" class="rounded-circle" width="32" height="32" alt="">
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#" class="text-default font-weight-semibold">{{ $item->title }}</a>
                                    </div>
                                </div>
                            </td>
                            <td><span class="text-success-600">{{ $item->user->name??'' }}</span></td>
                            <td><span class="text-muted">{{ $item->type->title??'' }}</span></td>
                            <td>
                                <h6 class="font-weight-semibold mb-0">
                                    {{collect($item->categories->pluck('title'))->join(', ')}}
                                </h6>
                            </td>
                            @if ($item->user&&$item->user->status == 1)
                            <td><span class="badge bg-blue">Đang hoạt động</span></td>
                            @else
                            <td><span class="badge bg-danger">Chưa duyệt</span></td>
                            @endif
                            <td class="text-center">
                                @if(auth()->user()->type==\App\Models\User::TYPE_SUPER_ADMIN)
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="{{ route('admin.store.approve', ['id'=> $item->id]) }}" class="dropdown-item"><i class="icon-file-stats"></i> Duyệt gian hàng</a>
                                            <a href="#" class="dropdown-item"><i class="icon-file-text2"></i> Xem thống kê gian hàng</a>
                                            <a href="#" class="dropdown-item"><i class="icon-file-locked"></i> Khóa gian hàng</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="{{ route('admin.store.edit',['id'=>$item->id]) }}" class="dropdown-item"><i class="icon-gear"></i> Chỉnh sửa gian hàng</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /marketing campaigns -->
    </div>

    <div class="col-xl-4">

        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Bài viết mới nhất</h6>
                <div class="header-elements">
                    <span class="font-weight-bold text-danger-600 ml-2">{{ $count_post }}</span>
                    <div class="list-icons ml-3">
                        <div class="list-icons-item dropdown">
                            <a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i></a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item"><i class="icon-sync"></i> Xem tất cả</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="media-list media-list-bordered">
            @foreach ($posts as $post)
                <li class="media">
                    <div class="mr-3">
                        <a href="#">
                            <img src="{{ asset($post->image) }}" class="rounded-circle" width="44" height="44" alt="">
                        </a>
                    </div>

                    <div class="media-body">
                        <span class="media-title d-block font-weight-semibold">{{ $post->title }}</span>
                        {{ $post->description }}
                    </div>
                </li>
            @endforeach
            </ul>
        </div>
    </div>
</div>
<!-- /dashboard content -->

</div>
<!-- /content area -->


@endsection
@section('scripts')
<script src="/global_assets/js/demo_pages/user_pages_profile.js"></script>

@endsection