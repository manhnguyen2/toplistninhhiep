@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Danh sách bài viết</h5>
                    <div class="header-elements">
                       <a class="badge badge-success" href="{{ route('admin.post.store') }}">Thêm bài viết</a>
                    </div>
                </div>

                <div class="card-body">
                

                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>Hình ảnh</th>
                            <th>Tiêu đề bài viết</th>
                            <th>Ngày cập nhật</th>
                            <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($posts as $post)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                            <a href="#">
                                                <img src="{{ asset($post->image) }}" class="rounded" width="100" height="100" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <a href="#" class="text-default font-weight-semibold">{{ $post->title }}</a>
                                        </div>
                                    </div>
                                </td>
                                <td><span class="mb-0">{{ date('H:i:s d/m/Y', strtotime($post->updated_at)) }}</span></td>

                                <td class="text-center">
                                    <div class="list-icons">
                                        <a href="{{ route('admin.post.edit', ['id'=> $post->id]) }}" class="icon-pencil5 text-danger text-bold"></a>
                                        <a href="{{ route('admin.post.destroy', ['id'=> $post->id]) }}" class="icon-trash text-danger"></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
