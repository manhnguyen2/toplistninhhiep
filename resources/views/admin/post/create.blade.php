@extends('layouts.admin')

@section('content')
<!-- Content area -->
<div class="content">
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Thêm bài viết mới</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
						<form method="POST" action="{{ route('admin.post.store') }}">
						@csrf
						<input type="hidden" name="id" value="">
							<div class="row">
								<div class="col-md-6">
										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Tiêu đề:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" placeholder="Nhập tên tiêu đề" value="" name="title">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-3">Mô tả</label>
											<div class="col-lg-9">
                                            <textarea rows="7" cols="5" class="form-control" name="description"></textarea>
											</div>
										</div>
                                        <div class="form-group row">
											<label class="col-lg-3 col-form-label">Thumbnail:</label>
											<div class="col-lg-9">
												<img class="img-responsive img-fluid" id="img-image" src="">
                                                <label class="custom-file">
														<input type="text" id="image" class="custom-file-input"  name="image">
														<span id="url-image" class="custom-file-label">Choose file</span>
													</label>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-3">Danh mục bài viết</label>
											<div class="col-lg-9">
												<select class="form-control" name="category_id">
													@foreach ($categories as $category)
													<option value="{{ $category->id }}">{{ $category->title }}</option>
													@endforeach
												</select>
											</div>
										</div>
								</div>
                                <div class="col-md-6">
										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Nội dung bài viết:</label>
											<div class="col-lg-12">
                                            <textarea name="content" id="content"></textarea>
                                            </div>
										</div>
								</div>
                                </div>
							<div class="text-right">
								<button type="submit" class="btn btn-primary">Cập nhật thông tin <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
	</div>
@endsection