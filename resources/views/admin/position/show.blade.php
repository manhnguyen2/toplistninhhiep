@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Danh mục bài viết</h5>
                    <div class="header-elements">
                       <a class="badge badge-success" href="{{ route('admin.position.store') }}">Thêm vị trí</a>
                    </div>
                </div>

                <div class="card-body">
                

                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Vị trí</th>
                            <th>Ngày cập nhật</th>
                            <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($positions as $position)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                            {{ $loop->index + 1}}
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <a href="#" class="text-default font-weight-semibold">{{ $position->title }}</a>
                                        </div>
                                    </div>
                                </td>
                                <td><span class="mb-0">{{ date('H:i:s d/m/Y', strtotime($position->updated_at)) }}</span></td>

                                <td class="text-center">
                                    <div class="list-icons">
                                        <a href="{{ route('admin.position.edit', ['id'=> $position->id]) }}" class="icon-pencil5 text-danger text-bold"></a>
                                        <a href="{{ route('admin.position.destroy', ['id'=> $position->id]) }}" class="icon-trash text-danger"></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
