<div class="table-responsive">
    <table class="mt-2 text-center table table-sm table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>STT</th>
                <th>Tên</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
            @foreach($records as $key => $record)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $record->name }}</td>
                <td>
                    <a href="javascript:;" class="text-warning" data-action="edit" data-id="{{ $record->id }}"><i class="icon-pencil3"></i></a>
                    <a href="javascript:;" class="text-danger" data-action="delete" data-id="{{ $record->id }}"><i class="icon-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
<div>
    {{ $records->links() }}
</div>