@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Danh mục vị trí</h5>
                    <div class="header-elements">
                       <a class="badge badge-success" href="javascript:;" data-id="0" data-action="edit">Thêm mới</a>
                    </div>
                </div>

                <div class="card-body" id="html">
                
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Xóa vị trí</h5>
            </div>
            <div class="modal-body">
                <p class="alert alert-danger">Bạn có chắc chắn muốn xóa không?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger" data-id=""> Xóa</button>
            </div>
        </div>
    </div>
</div>
<div id="modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="modal-body" enctype="multipart/form-data">
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function search() {
        $.ajax({
            url:'{{route('admin.province.index')}}',method:'POST',
            success:function(resp) {
                $('#html').html(resp)
            }
        })
    }
    $(function(){
        search()
        $('body').delegate('#modal form','submit',function(e){
            e.preventDefault()
            $.ajax({
                url:'{{route('admin.province.form')}}',method:'POST',
                contentType:false,processData:false,data:new FormData(this),
                success:function(resp) {
                    if (resp.success) {
                        toastr['success']('Cập nhật thành công')
                        $('#modal').modal('hide')
                        search()
                    }
                }
            })
        })
        $('body').delegate('#modal_delete [type=submit]','click',function(e){
            e.preventDefault()
            $.ajax({
                url:'{{route('admin.province.delete')}}?id='+$(this).data('id'),method:'POST',
                success:function(resp) {
                    if (resp.success) {
                        toastr['success']('Xóa thành công')
                        $('#modal_delete').modal('hide')
                        search()
                    }
                }
            })
        })
        $('body').delegate('[data-action="delete"]','click',function(){
            $('#modal_delete [data-id]').data('id',this.dataset.id)
            $('#modal_delete').modal('show')
        })
        $('body').delegate('[data-action="edit"]','click',function(){
            $.ajax({
                url:'{{route('admin.province.form')}}?id='+this.dataset.id,
                success:function(resp) {
                    $('#modal .modal-body').html(resp)
                    $('#modal').modal('show')
                    $('#modal .form-input-styled').uniform();
                }
            })
        })
        console.log('Doc ready')
    })
</script>
@endsection