    <input type="hidden" name="id" value="{{ $record->id ?? '' }}">
    <input type="hidden" name="modal" value="true">
    <div class="form-group row">
        <div class="col-3">
            <label><span class="required"></span> Tên</label>
        </div>
        <div class="col-9">
            <input type="text" required value="{{ $record->name ?? '' }}" class="form-control" name="name">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-3">
            <label>Ảnh</label>
        </div>
        <div class="col-9">
            <input type="file" name="file_upload" class="form-input-styled">
            <input type="hidden" value="{{$record->image??''}}" name="image">
            <img class="mt-2" src="{{$record->image??''}}" height="50">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-3">
        </div>
        <div class="col-9">
            <button type="submit" class="btn btn-primary"> Lưu</button>
        </div>
    </div>