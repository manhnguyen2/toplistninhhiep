<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body">
                <div class="card-body text-center">
                    <a href="#">
                        <img src="{{asset('global_assets/images/placeholders/placeholder.jpg')}}" class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="">
                    </a>
                    <h6 class="mb-0 text-white text-shadow-dark">{{ auth()->user()->name }}</h6>
                    <span class="font-size-sm text-white text-shadow-dark">{{ auth()->user()->email }}</span>
                </div>
                                            
                <div class="sidebar-user-material-footer">
                    <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>My account</span></a>
                </div>
            </div>

            <div class="collapse" id="user-nav">
                <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-user-plus"></i>
                            <span>My profile</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-coins"></i>
                            <span>My balance</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-comment-discussion"></i>
                            <span>Messages</span>
                            <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-cog5"></i>
                            <span>Account settings</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.logout') }}" class="nav-link">
                            <i class="icon-switch2"></i>
                            <span>Đăng xuất</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Menu</div> <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}" class="d-block nav-link @if (Route::current()->getName() == 'admin.dashboard') active @endif">
                        <i class="icon-home4"></i>
                            Dashboard
                    </a>
                </li>
                @if(auth()->user()->type==\App\Models\User::TYPE_SUPER_ADMIN)
                <li class="nav-item">
                    <a href="{{ route('admin.store.show') }}" class="d-block nav-link @if (Route::current()->getName() == 'admin.store.show') active @endif">
                        <i class="icon-store2"></i>

                            Quản lý gian hàng
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.category.show') }}" class="d-block nav-link @if (Route::current()->getName() == 'admin.category.show') active @endif">
                        <i class="icon-store"></i>
                            Quản lý danh mục gian hàng
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.user.index') }}" class="d-block nav-link @if (Route::current()->getName() == 'admin.user.show') active @endif">
                        <i class="icon-user"></i>
                            Quản lý thành viên

                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a href="{{ route('admin.post.show') }}" class="d-block nav-link @if (Route::current()->getName() == 'admin.post.show') active @endif">
                        <i class="icon-magazine"></i>
                            Quản lý bài viết
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.postcategory.show') }}" class="d-block nav-link @if (Route::current()->getName() == 'admin.postcategory.show') active @endif">
                        <i class="icon-file-text2"></i>
                            Quản lý danh mục bài viết
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.position.show') }}" class="d-block nav-link @if (Route::current()->getName() == 'admin.position.show') active @endif">
                        <i class="icon-map5"></i>
                            Quản lý danh mục vị trí
                    </a>
                </li>
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
</div>