
@if (Session::get('success'))
<div class="alert alert-success border-0 alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    {{ Session::get('success') }}
</div>
@endif