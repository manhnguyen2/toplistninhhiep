@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Danh sách tài khoản</h5>
                    <div class="header-elements">
                        <!-- <a class="badge badge-suc">Thêm thành viên mới</a> -->
                    </div>
                </div>

                <div class="card-body">
                
                    <form action="">
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <select name="status" class="form-control select">
                                        <option value="">Tất cả</option>
                                        <option {{ request('status') == 1 ? 'selected' : '' }} value="1">Đã duyệt</option>
                                        <option {{ request('status') == 0 ? 'selected' : '' }} value="0">Chờ duyệt</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <button type="submit" class="btn btn-success"> Lọc</button>
                            </div>
                        </div>
                    </form>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tên thành viên</th>
                                <th>Email/SĐT</th>
                                <th>Loại tài khoản</th>
                                <th>Cập nhật</th>
                                <th>Trạng thái</th>
                                <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="mr-3">
                                                <a href="#">
                                                    <img src="{{ asset($user->avatar) }}" class="rounded-circle" width="32" height="32" alt="">
                                                </a>
                                            </div>
                                            <div>
                                                <a href="#" class="text-default font-weight-semibold">{{ $user->name }}</a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div>
                                                <a href="javascript:;" class="text-default font-weight-semibold">{{ $user->email }}</a>
                                                <a href="javascript:;" title="{{ $user->phone_verified_at?'Đã xác thực':'Chưa xác thực'}}" class="text-default font-weight-semibold">{{ $user->username }}</a>
                                            </div>
                                        </div>
                                    </td>
                                    @if ($user->type == 1)
                                    <td><span class="badge bg-blue">Admin</span></td>
                                    @elseif ($user->type == 2)
                                    <td><span class="badge bg-indigo">Thành viên gian hàng</span></td>
                                    @else
                                    <td><span class="badge bg-green">Thành viên thường</span></td>
                                    @endif
                                    <td><span class="mb-0">{{ date('H:i:s d/m/Y', strtotime($user->updated_at)) }}</span></td>
                                    @if ($user->status == 1)
                                    <td><span class="badge bg-blue">Đang hoạt động</span></td>
                                    @else
                                    <td><span class="badge bg-danger">Chưa duyệt</span></td>
                                    @endif
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="list-icons-item dropdown">
                                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{ route('admin.user.approve', ['id' => $user->id]) }}" class="dropdown-item"><i class="icon-file-stats"></i> Duyệt thành viên</a>
                                                    @if ($user->type == 2)
                                                    <a href="{{ route('admin.store.edit', ['id'=> $user->store->id]) }}" class="dropdown-item"><i class="icon-file-locked"></i> Sửa thông tin gian hàng</a>
                                                    @endif
                                                    <div class="dropdown-divider"></div>
                                                    <a href="{{ route('admin.user.edit',['id'=>$user->id]) }}" class="dropdown-item"><i class="icon-gear"></i> Chỉnh sửa thông tin</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div>
                    {{$users->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
