@extends('layouts.admin')

@section('content')
<!-- Content area -->
<div class="content">
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Chỉnh sửa thông tin tài khoản</h5>
						<div class="header-elements">
	                	</div>
					</div>

					<div class="card-body">
						<form method="POST" action="{{ route('admin.user.update') }}">
						@csrf
						<input type="hidden" name="id" value="{{ $data->id }}">
						    <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Tên thành viên:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" value="{{ $data->name }}" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Email:</label>
                                <div class="col-lg-9">
                                    <input type="email" class="form-control" value="{{ $data->email }}" name="email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Avatar:</label>
                                <div class="col-lg-9">
                                    <img height="100" id="img-avatar" src="{{ asset($data->avatar) }}">
                                    <label class="custom-file">
                                        <input type="text" id="avatar" class="custom-file-input" value=" {{ $data->avatar }}" name="avatar">
                                        <span id="url-avatar" class="custom-file-label">Choose file</span>
                                    </label>
                                </div>
                            </div>
							<div class="text-right">
								<button type="submit" class="btn btn-primary">Cập nhật thông tin <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
	</div>
@endsection