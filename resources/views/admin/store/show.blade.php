@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Danh sách cửa hàng</h5>
                    <div class="header-elements">
                       <a class="badge badge-success" href="{{ route('admin.store.create') }}">Thêm gian hàng</a>
                    </div>
                </div>

                <div class="card-body">
                

                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>Tên cửa hàng</th>
                            <th>Tên khách hàng</th>
                            <th>Loại cửa hàng</th>
                            <th>Danh mục</th>
                            <th>Cập nhật</th>
                            <th>Trạng thái</th>
                            <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($stores as $item)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="mr-3">
                                            <a href="#">
                                                <img src="{{ asset($item->avatar) }}" class="rounded-circle" width="32" height="32" alt="">
                                            </a>
                                        </div>
                                        <div>
                                            <a href="#" class="text-default font-weight-semibold">{{ $item->title }}</a>
                                        </div>
                                    </div>
                                </td>
                                <td><span class="text-success-600">{{ $item->user?$item->user->name:'' }}</span></td>
                                <td><span class="text-muted">{{ $item->type ? $item->type->title : '' }}</span></td>
                                <td>
                                @foreach ($item->categories as $category)    
                                <h6 class="font-weight-semibold mb-0">{{ $category->title }}</h6>
                                @endforeach
                                </td>
                                <td><span class="mb-0">{{ date('H:i:s d/m/Y', strtotime($item->updated_at)) }}</span></td>
                                @if ($item->user && $item->user->status == 1)
                                <td><span class="badge bg-blue">Đang hoạt động</span></td>
                                @else
                                <td><span class="badge bg-danger">Chưa duyệt</span></td>
                                @endif
                                <td class="text-center">
                                    <div class="list-icons">
                                        <div class="list-icons-item dropdown">
                                            <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="{{ route('admin.store.approve', ['id'=> $item->id]) }}" class="dropdown-item"><i class="icon-file-stats"></i> Duyệt gian hàng</a>
                                                <a href="#" class="dropdown-item"><i class="icon-file-text2"></i> Xem thống kê gian hàng</a>
                                                <a href="#" class="dropdown-item"><i class="icon-file-locked"></i> Khóa gian hàng</a>
                                                <div class="dropdown-divider"></div>
                                                <a href="{{ route('admin.store.edit',['id'=>$item->id]) }}" class="dropdown-item"><i class="icon-gear"></i> Chỉnh sửa gian hàng</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
