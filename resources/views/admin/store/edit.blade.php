@extends('layouts.admin')

@section('content')
<!-- Content area -->
<div class="content">
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Chỉnh sửa thông tin gian hàng</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
						<form method="POST" action="{{ route('admin.store.update') }}" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="id" value="{{ $data->id }}">
							<div class="row">
								<div class="col-md-6">
									<fieldset>
										<legend class="font-weight-semibold"><i class="icon-pencil7 mr-2"></i> Thông tin chính</legend>

										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Tên gian hàng:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" placeholder="Nhập tên gian hàng" value="{{ $data->title }}" name="title">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-3">Loại gian hàng</label>
											<div class="col-lg-9">
												<select class="form-control select" name="type_id">
													@foreach ($types as $type)
													<option value="{{ $type->id }}" @if($type->id == $data->type_id) selected @endif>{{ $type->title }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-3">Danh mục gian hàng</label>
											<div class="col-lg-9">
												@foreach ($categories as $category)
												<label>
													@if($data->categories()->where('category_id', $category->id)->count())
													<input checked type="checkbox" name="category_id[]" value="{{ $category->id }}">{{ $category->title }}
													@else
													<input type="checkbox" name="category_id[]" value="{{ $category->id }}">{{ $category->title }}
													@endif
												</label>
												@endforeach
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-3">Vị trí</label>
											<div class="col-lg-9">
												<select class="form-control select-search" name="position_id">
													@foreach ($positions as $position)
													<option value="{{ $position->id }}" @if($position->id == $data->position_id) selected @endif>{{ $position->title }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Địa chỉ:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" placeholder="" value="{{ $data->address }}" name="address">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Số điện thoại</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" value="{{ $data->phone }}" name="phone">
											</div>
										</div>


										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Mô tả:</label>
											<div class="col-lg-9">
												<textarea rows="7" cols="5" class="form-control" name="description">{{ $data->description }}</textarea>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Nội dung chi tiết:</label>
											<div class="col-lg-9">
												<textarea rows="7" id="content" cols="5" class="form-control ckeditor" name="content">{{ $data->content }}</textarea>
											</div>
										</div>
									</fieldset>
								</div>

								<div class="col-md-6">
									<fieldset>
					                	<legend class="font-weight-semibold"><i class="icon-images3 mr-2"></i> Hình ảnh</legend>                                        
										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Ảnh đại diện:</label>
											<div class="col-lg-9">
												<input type="file" class="form-control-file" onchange="loadFile(event)" name="file_upload" accept="image/*">
												<img class="img-selector mt-1" id="output" src="{{$data->avatar}}" style="height:100px;">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label">Banner:</label>
											<div class="col-lg-9">
												<input type="file" class="form-control-file" onchange="loadFile(event,'output_banner')" name="file_banner_upload" accept="image/*">
												<img class="img-selector mt-1" id="output_banner" src="{{collect($data->banner)->first()?:''}}" style="height:100px;">
											</div>
										</div>
									</fieldset>
								</div>
							</div>

							<div class="text-right">
								<button type="submit" class="btn btn-primary">Cập nhật thông tin <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
	</div>
@endsection