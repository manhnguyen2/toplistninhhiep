@extends('layouts.admin')

@section('content')
<!-- Content area -->
<div class="content">
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Thêm danh mục bài viết</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
						<form method="POST" action="{{ route('admin.postcategory.store') }}" enctype="multipart/form-data">
							@csrf
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-lg-3 col-form-label">Tiêu đề:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Nhập tên tiêu đề" value="" name="title">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3">Mô tả</label>
										<div class="col-lg-9">
										<textarea rows="7" cols="5" class="form-control" name="description"></textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3">Danh mục cha</label>
										<div class="col-lg-9">
											<select name="parent_id" class="form-control">
												<option value="">Chọn</option>
												@foreach(\App\Models\PostCategory::whereNull('parent_id')->get() as $parent)
												<option value="{{ $parent->id }}">{{ $parent->title }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-lg-3 col-form-label">Thumbnail:</label>
										<div class="col-lg-9">
											<label class="col-lg-3 col-form-label">Ảnh đại diện:</label>
											<div class="col-lg-9">
												<input type="file" class="form-control-file" onchange="loadFile(event)" name="file_upload" accept="image/*">
												<img class="img-selector mt-1" id="output" src="" style="height:100px;">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="text-right">
								<button type="submit" class="btn btn-primary">Cập nhật thông tin <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
	</div>
@endsection