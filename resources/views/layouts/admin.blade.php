<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('admin.partials.head')
@include('ckfinder::setup')
<body class="">
    @include('admin.partials.navbar')
	<div class="page-content">
        @include('admin.partials.sidebar')
		<div class="content-wrapper">
            @include('admin.partials.messages')
            @yield('content')
            @include('admin.partials.footer')
		</div>
	</div>
    <script>
        var loadFile = function(event,elm = 'output') {
            var output = document.getElementById(elm);
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
    @section('scripts')
    @show
</body>
</html>
