<!DOCTYPE HTML>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('partials.head')
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            @include('partials.header')
            <!-- wrapper-->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    @yield('content')
                </div>
                <!--content end-->
            </div>
            @include('partials.footer')   
        </div>
        @include('partials.scripts')
        @section('scripts')
        @show
    </body>

</html>