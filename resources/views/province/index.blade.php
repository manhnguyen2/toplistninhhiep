@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{$record->name}}"/>
<meta name="description" content="{{$record->name}}"/>
<meta property="og:image" content="{{env('APP_URL').$record->image}}">
<meta property="og:type" content="article">
<meta property="og:title" content="{{$record->name}}">
<meta property="og:description" content="{{$record->name}}">
@stop
@section('content')
<section>
    <div class="container big-container">
        <div class="section-title">
            <h2><span>{{$record->name}}</span></h2>
            <div class="section-subtitle">{{$record->name}}</div>
            <span class="section-separator"></span>
        </div>
        <div class="grid-item-holder gallery-items fl-wrap">
            @include('shared.stores')
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Province index ready')
    })
</script>
@endsection