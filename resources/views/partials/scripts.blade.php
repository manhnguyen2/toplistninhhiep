
<!--=============== scripts  ===============-->
<script src="/themes/townhub/js/jquery.min.js"></script>
<script src="/themes/townhub/js/plugins.js"></script>
<script src="/themes/townhub/js/scripts.js"></script>
<script src="/js/toastr.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwJSRi0zFjDemECmFl9JtRj1FY7TiTRRo&amp;libraries=places&amp;callback=initAutocomplete"></script>        
<script src="/themes/townhub/js/map-single.js"></script>
<script>
    function search() {
        window.location.href = '/category/listing.html?se=' + $('.header-search-input input').val()
    }
    $(function(){
        $('#form_subscribe').submit(function(e){
            e.preventDefault()
            $.ajax({
                url:'{{route('subscribe')}}',method:'POST',
                contentType:false,processData:false,
                data:new FormData(this),
                success:function(resp) {
                    toastr['info']('Đăng ký thành công')
                }
            })
        })
    })
</script>
@guest
<script>
    $(function(){
        $('[name="registerform"]').submit(function(e){
            e.preventDefault()
            $.ajax({
                url:'{{ route('auth.register') }}',method:'POST',data:new FormData(this),
                contentType:false,processData:false,
                success:function(resp) {
                    if (resp) {
                        if (resp.sent) {
                            toastr['warning']('Hãy nhập mã OTP để xác nhận số điện thoại')
                            $('[data-otp="true"]').toggle(true)
                        } else if (resp.sent_failed) {
                            toastr['error']('Mã OTP không chính xác')
                        } else {
                            toastr['success']('Đăng ký thành công')
                            setTimeout(function(){
                                if (resp.redirect == 'shop') {
                                    location.href = '/profile/edit'
                                } else {
                                    location.href = '/home'
                                }
                            }, 300)
                        }
                    }
                },
                error:function(x,h,r) {
                    if (x.status==422 && x.responseJSON && x.responseJSON.errors) {
                        Object.keys(x.responseJSON.errors).forEach(prop => {
                            toastr['error'](x.responseJSON.errors[prop])
                        })
                    }
                }
            })
        })
        $('[name="loginform"]').submit(function(e){
            e.preventDefault()
            $.ajax({
                url:'{{ route('login') }}',method:'POST',data:new FormData(this),
                contentType:false,processData:false,
                success:function(resp) {
                    if (resp) {
                        toastr['success']('Đăng nhập thành công')
                        setTimeout(function(){
                            location.href = '/home'
                        }, 300)
                    }
                },
                error:function(x,h,r) {
                    if (x.status==422 && x.responseJSON && x.responseJSON.errors) {
                        Object.keys(x.responseJSON.errors).forEach(prop => {
                            toastr['error'](x.responseJSON.errors[prop])
                        })
                    }
                }
            })
        })
    })
</script>
@endguest