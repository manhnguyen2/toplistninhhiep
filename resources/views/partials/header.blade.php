<header class="main-header dsh-header">
    <!-- logo-->
    <a href="/" class="logo-holder"><img src="/img/logo.png" alt="{{env('APP_NAME')}}"></a>
    <!-- logo end-->
    <!-- header-search_btn-->         
    <div class="header-search_btn show-search-button"><i class="fal fa-search"></i><span>Bạn đang tìm kiếm điều gì?</span></div>
    <!-- header-search_btn end-->
    <!-- header opt --> 
    @auth
    <div class="header-user-menu">
        <div class="header-user-name">
            <span><img src="{{ auth()->user()->avatar ?: '/img/no-image.jpg' }}" alt="{{ auth()->user()->name }}"></span>
            {{ auth()->user()->name }}
        </div>
        <ul>
            <li><a href="{{ route('profile') }}"> Thông tin cá nhân</a></li>
            <li><a href="{{ route('logout') }}">Thoát</a></li>
        </ul>
    </div>
    @endauth
    @guest
    <a href="{{ route('login') }}" class="add-list color-bg">Đăng nhập <span><i class="fal fa-layer-plus"></i></span></a>
    <div class="show-reg-form modal-open avatar-img" data-srcav="images/avatar/3.jpg"><i class="las la-user"></i>Đăng ký</div>
    @endguest
    <div class="nav-button-wrap color-bg">
        <div class="nav-button">
            <span></span><span></span><span></span>
        </div>
    </div>
    <!-- nav-button-wrap end-->
    <!--  navigation --> 
    <div class="nav-holder main-menu">
        <nav>
            <ul class="no-list-style">
                <li>
                    <a href="/" class="act-link">Trang chủ</a>
                </li>
                <li>
                    <a href="javascript:;">Danh sách cửa hàng <i class="fa fa-caret-down"></i></a>
                    <!--second level -->   
                    <ul>
                        @foreach(\App\Models\Category::whereNull('parent_id')->get() as $item)
                        @if($item->children()->count())
                        <li>
                            <a href="{{$item->url}}">{{$item->title}} <i class="fa fa-caret-down"></i></a>
                            <!--third  level  -->
                            <ul>
                                @foreach($item->children as $child)
                                <li><a href="{{$child->url}}">{{$child->title}}</a></li>
                                @endforeach
                            </ul>
                            <!--third  level end-->
                        </li>
                        @else
                        <li><a href="{{$item->url}}">{{$item->title}}</a></li>
                        @endif
                        @endforeach
                    </ul>
                    <!--second level end-->
                </li>
                <li>
                    <a href="javascript:;">Tin tức <i class="fa fa-caret-down"></i></a>
                    <ul>
                        @foreach(\App\Models\PostCategory::whereNull('parent_id')->get() as $item)
                        <li><a href="{{$item->url}}">{{$item->title}}</a></li>
                        @endforeach
                    </ul>
                </li>
                @foreach(\App\Models\Post::where('category_id',100)->take(2)->get() as $item)
                <li>
                    <a href="{{$item->url}}" class="act-link">{{$item->title}}</a>
                </li>
                @endforeach
                @foreach(\App\Models\PostCategory::where('slug','kinh-nghiem-kinh-doanh')->take(1)->get() as $item)
                <li>
                    <a href="{{$item->url}}" class="act-link">{{$item->title}}</a>
                </li>
                @endforeach
            </ul>
        </nav>
    </div>
    <form action="/tim-kiem">
        <div class="header-search_container header-search vis-search">
            <div class="container small-container">
                <div class="header-search-input-wrap fl-wrap">
                    <!-- header-search-input --> 
                    <div class="header-search-input">
                        <label><i class="fal fa-keyboard"></i></label>
                        <input type="text" name="keyword" placeholder="Từ khóa"   value=""/>  
                    </div>
                    <div class="header-search-input header-search_selectinpt ">
                        <select name="position" data-placeholder="Vị trí" class="chosen-select no-radius" >
                            <option>Tất cả</option>
                            @foreach(\App\Models\Position::all() as $item)
                            <option value="{{$item->id}}">{{$item->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="header-search-input header-search_selectinpt ">
                        <select name="type" data-placeholder="Loại gian hàng" class="chosen-select no-radius" >
                            <option>Tất cả</option>
                            <option value="1">Nguyên liệu</option>
                            <option value="2">Thời trang</option>
                            <option value="3">Xưởng</option>
                        </select>
                    </div>
                    <!-- header-search-input end --> 
                    <button class="header-search-button green-bg" onclick="search()"><i class="far fa-search"></i> Tìm </button>
                </div>
                <div class="header-search_close color-bg"><i class="fal fa-long-arrow-up"></i></div>
            </div>
        </div>
    </form>
    @if(0)
    <div class="header-modal novis_wishlist">
        <!-- header-modal-container--> 
        <div class="header-modal-container scrollbar-inner fl-wrap" data-simplebar>
            <!--widget-posts-->
            <div class="widget-posts  fl-wrap">
                <ul class="no-list-style">
                    <li>
                        <div class="widget-posts-img"><a href="listing-single.html"><img src="/themes/townhub/images/gallery/thumbnail/1.png" alt=""></a>  
                        </div>
                        <div class="widget-posts-descr">
                            <h4><a href="listing-single.html">Iconic Cafe</a></h4>
                            <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> 40 Journal Square Plaza, NJ, USA</a></div>
                            <div class="widget-posts-descr-link"><a href="listing.html" >Restaurants </a>   <a href="listing.html">Cafe</a></div>
                            <div class="widget-posts-descr-score">4.1</div>
                            <div class="clear-wishlist"><i class="fal fa-times-circle"></i></div>
                        </div>
                    </li>
                    <li>
                        <div class="widget-posts-img"><a href="listing-single.html"><img src="/themes/townhub/images/gallery/thumbnail/2.png" alt=""></a>
                        </div>
                        <div class="widget-posts-descr">
                            <h4><a href="listing-single.html">MontePlaza Hotel</a></h4>
                            <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> 70 Bright St New York, USA </a></div>
                            <div class="widget-posts-descr-link"><a href="listing.html" >Hotels </a>  </div>
                            <div class="widget-posts-descr-score">5.0</div>
                            <div class="clear-wishlist"><i class="fal fa-times-circle"></i></div>
                        </div>
                    </li>
                    <li>
                        <div class="widget-posts-img"><a href="listing-single.html"><img src="/themes/townhub/images/gallery/thumbnail/3.png" alt=""></a>
                        </div>
                        <div class="widget-posts-descr">
                            <h4><a href="listing-single.html">Rocko Band in Marquee Club</a></h4>
                            <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>75 Prince St, NY, USA</a></div>
                            <div class="widget-posts-descr-link"><a href="listing.html" >Events</a> </div>
                            <div class="widget-posts-descr-score">4.2</div>
                            <div class="clear-wishlist"><i class="fal fa-times-circle"></i></div>
                        </div>
                    </li>
                    <li>
                        <div class="widget-posts-img"><a href="listing-single.html"><img src="/themes/townhub/images/gallery/thumbnail/4.png" alt=""></a>
                        </div>
                        <div class="widget-posts-descr">
                            <h4><a href="listing-single.html">Premium Fitness Gym</a></h4>
                            <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> W 85th St, New York, USA</a></div>
                            <div class="widget-posts-descr-link"><a href="listing.html" >Fitness</a> <a href="listing.html" >Gym</a> </div>
                            <div class="widget-posts-descr-score">5.0</div>
                            <div class="clear-wishlist"><i class="fal fa-times-circle"></i></div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- widget-posts end-->
        </div>
        <!-- header-modal-container end--> 
        <div class="header-modal-top fl-wrap">
            <h4>Your Wishlist : <span><strong></strong> Locations</span></h4>
            <div class="close-header-modal"><i class="far fa-times"></i></div>
        </div>
    </div>
    @endif
</header>