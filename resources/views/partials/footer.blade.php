
<!--footer -->
<footer class="main-footer fl-wrap">
    <div class="footer-inner   fl-wrap">
        <div class="container">
            <div class="row">
                <!-- footer-widget-->
                <div class="col-md-4">
                    <div class="footer-widget fl-wrap">
                        <div class="footer-logo"><a href="/"><img src="/img/logo.png" alt="{{env('APP_NAME')}}"></a></div>
                        <h3>CÔNG TY TNHH TIOT</h3>
                        <div class="footer-contacts-widget fl-wrap">
                            <p>Mã số thuế: 0109741870 do Sở Kế hoạch và Đầu tư thành phố Hà Nội cấp ngày 06/09/2021
                                <br>Người đại diện theo pháp luật: NGUYỄN HUY NGỌC, Chức danh: Giám đốc
                                <br>Địa chỉ trụ sở: Thôn 3, Xã Ninh Hiệp, Huyện Gia Lâm, Thành phố Hà Nội, Việt Nam</p>
                                <p>Hotline: <a href="tel:0968681997">09.6868.1997</a></p>
                                <p>Email: <a href="mailto:toplistninhhiep@gmail.com">toplistninhhiep@gmail.com</a></p>
                            <div class="footer-social">
                                <ul class="no-list-style">
                                    <li><a href="https://www.facebook.com/Ch%E1%BB%A3-Ninh-Hi%E1%BB%87p-review-th%E1%BB%9Di-trang-112764807894808" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-google"></i></a></li>
                                    <li><a href="https://www.youtube.com/channel/UCwoCTJtfwJPV8AmR5TqKY3g/featured" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-widget fl-wrap">
                        <h3>Chính sách</h3>
                        <div class="footer-widget-posts fl-wrap">
                            <ul class="list-unstyled">
                                @foreach(\App\Models\Post::latest()->where('category_id',14)->take(10)->get() as $item)
                                <li class="">
                                    <a href="{{$item->url}}"  class="">{{$item->title}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <h3>Facebook</h3>
                        <div class="footer-widget-posts fl-wrap">
                            <ul class="list-unstyled">
                                <li><a href="https://www.facebook.com/groups/choninhhiepbansi" target="_blank"  class="">Chợ Ninh Hiệp – Quần áo – phụ kiện</a></li>
                                <li><a href="https://www.facebook.com/groups/chovaininhhiepmaymac" target="_blank"  class="">Chợ vải Ninh Hiệp may mặc</a></li>
                                <li><a href="https://www.facebook.com/Ch%E1%BB%A3-Ninh-Hi%E1%BB%87p-review-th%E1%BB%9Di-trang-112764807894808" target="_blank"  class="">Chợ Ninh Hiệp – Review thời trang</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-widget fl-wrap">
                        <h3>Danh mục gian hàng</h3>
                        <div class="footer-widget-posts fl-wrap">
                            <ul class="list-unstyled">
                                @foreach(\App\Models\Category::orderBy('title','asc')->take(30)->get() as $item)
                                <li class="">
                                    <a href="{{$item->url}}"  class="">{{$item->title}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-widget fl-wrap">
                        <h3>ĐĂNG KÝ NHẬN TIN</h3>
                        <div class="footer-widget-posts fl-wrap">
                            <ul class="list-unstyled">
                                <li>Hãy nhập địa chỉ email của bạn vào ô dưới đây để có thể nhận được tất cả các tin tức mới nhất từ Toplist Ninh Hiệp. Chúng tôi xin đảm bảo sẽ không gửi mail rác, mail spam tới bạn.</li>
                            </ul>
                            <form id="form_subscribe">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="email" class="form-control" placeholder="Email" aria-label="Email">
                                        <div class="input-group-append">
                                            <button class="btn btn-send" type="submit">Đăng ký</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            @if(0)
                            <a href="javascript:;">
                                <img src="/img/bct.jpg" alt="bct">
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer bg-->
        <div class="footer-bg" data-ran="4"></div>
        <div class="footer-wave">
            <svg viewbox="0 0 100 25">
                <path fill="#fff" d="M0 30 V12 Q30 17 55 12 T100 11 V30z" />
            </svg>
        </div>
        <!-- footer bg  end-->
    </div>
    <!--footer-inner end -->
    <!--sub-footer-->
    <div class="sub-footer  fl-wrap">
        <div class="container">
            <div class="copyright">Thiết kế bởi Tiot.vn</div>
            <div class="subfooter-nav">
                <ul class="no-list-style">
                    <li><a href="javascript:;">Điều khoản sử dụng</a></li>
                    <li><a href="javascript:;">Chính sách bảo mật</a></li>
                    <li><a href="javascript:;">Tin tức</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--sub-footer end -->
</footer>
<!--map-modal -->
<div class="map-modal-wrap">
    <div class="map-modal-wrap-overlay"></div>
    <div class="map-modal-item">
        <div class="map-modal-container fl-wrap">
            <div class="map-modal fl-wrap">
                <div id="singleMap" data-latitude="40.7" data-longitude="-73.1"></div>
            </div>
            <h3><span>Location for : </span><a href="#">Listing Title</a></h3>
            <div class="map-modal-close"><i class="fal fa-times"></i></div>
        </div>
    </div>
</div>
<!--map-modal end -->                
<!--register form -->
<div class="main-register-wrap modal">
    <div class="reg-overlay"></div>
    <div class="main-register-holder tabs-act">
        <div class="main-register fl-wrap  modal_main">
            <div class="main-register_title">{{env('APP_NAME')}}</div>
            <div class="close-reg"><i class="fal fa-times"></i></div>
            <ul class="tabs-menu fl-wrap no-list-style">
                <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> Đăng nhập</a></li>
                <li><a href="#tab-2"><i class="fal fa-user-plus"></i> Đăng ký</a></li>
            </ul>
            <!--tabs -->                       
            <div class="tabs-container">
                <div class="tab">
                    <!--tab -->
                    <div id="tab-1" class="tab-content first-tab">
                        <div class="custom-form">
                            <form method="post"  name="loginform">
                                @csrf
                                <label>Email <span>*</span> </label>
                                <input name="email" type="text"   onClick="this.select()" value="">
                                <label >Mật khẩu <span>*</span> </label>
                                <input name="password" type="password"   onClick="this.select()" value="" >
                                <button type="submit"  class="btn float-btn color2-bg"> Đăng nhập <i class="fas fa-caret-right"></i></button>
                                <div class="clearfix"></div>
                                <div class="filter-tags">
                                    <input id="check-a3" type="checkbox" name="check">
                                    <label for="check-a3">Ghi nhớ</label>
                                </div>
                            </form>
                            <div class="lost_password">
                                <a href="#">Quên mật khẩu?</a>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                    <!--tab -->
                    <div class="tab">
                        <div id="tab-2" class="tab-content">
                            <div class="custom-form">
                                <form method="post"   name="registerform" class="main-register-form" id="main-register-form2">
                                    @csrf
                                    <label >Số điện thoại <span>*</span> </label>
                                    <input name="username" type="text"   onClick="this.select()" value="">
                                    <label >Tên cửa hàng </label>
                                    <input name="shop_name" type="text"   onClick="this.select()" value="">

                                    <label>Email <span>*</span></label>
                                    <input name="email" type="text"  onClick="this.select()" value="">
                                    <label >Mật khẩu <span>*</span></label>
                                    <input name="password" type="password"   onClick="this.select()" value="">
                                    <label style="display:none;" data-otp="true">OTP <span>*</span></label>
                                    <input style="display:none;" data-otp="true"name="otp" type="text"   onClick="this.select()" value="">
                                    <div class="filter-tags ft-list">
                                        <input id="check-a2" type="checkbox" name="check_shop">
                                        <label for="check-a2">Đăng ký cửa hàng</label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="filter-tags ft-list">
                                        <input id="check-a" type="checkbox" name="check">
                                        <label for="check-a">Tôi đồng ý với <a href="#">điều khoản sử dụng</a> của {{env('APP_NAME')}}</label>
                                    </div>
                                    <div class="clearfix"></div>
                                    <button type="submit"     class="btn float-btn color2-bg"> Đăng ký  <i class="fas fa-caret-right"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                </div>
                <!--tabs end -->
                <div class="log-separator text-center fl-wrap"><span>hoặc</span></div>
                <div class="soc-log fl-wrap text-center">
                    <p>sử dụng tài khoản mạng xã hội.</p>
                    <a href="{{route('login-facebook')}}" class="facebook-log"> Facebook</a>
                </div>
                <div class="wave-bg">
                    <div class='wave -one'></div>
                    <div class='wave -two'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--register form end -->
<a class="to-top"><i class="fas fa-caret-up"></i></a>  
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v12.0&appId=583344536079141&autoLogAppEvents=1" nonce="bIzsPMaV"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61fdf8199bd1f31184db0d51/1fr41295q';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->