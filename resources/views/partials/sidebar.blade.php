<div class="box-widget-wrap fl-wrap fixed-bar">
    <div class="box-widget-item fl-wrap block_box">
        <div class="box-widget-item-header">
            <h3>Tìm kiếm</h3>
        </div>
        <div class="box-widget fl-wrap">
            <div class="box-widget-content">
                <div class="search-widget">
                    <form class="fl-wrap">
                        <input name="se" id="se" type="text" class="search" placeholder="Từ khóa.." value="" />
                        <button class="search-submit color2-bg" id="submit_btn"><i class="fal fa-search"></i> </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="box-widget-item fl-wrap block_box">
        <div class="box-widget-item-header">
            <h3>Bài viết nổi bật</h3>
        </div>
        <div class="box-widget  fl-wrap">
            <div class="box-widget-content">
                <div class="widget-posts  fl-wrap">
                    <ul class="no-list-style">
                        @foreach(\App\Models\Post::orderBy('viewed','desc')->take(5)->get() as $post)
                        <li>
                            <div class="widget-posts-img"><a href="{{$post->url}}"><img src="{{$post->image?:'/themes/townhub/images/gallery/thumbnail/1.png'}}" alt="{{$post->title}}"></a></div>
                            <div class="widget-posts-descr">
                                <h4><a href="{{$post->url}}">{{$post->title}}</a></h4>
                                <div class="geodir-category-location fl-wrap"><a href="{{$post->url}}"><i class="fal fa-calendar"></i> {{date('d/m/Y',strtotime($post->created_at))}}</a></div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="box-widget-item fl-wrap block_box">
        <div class="box-widget-item-header">
            <h3>Danh mục phổ biến</h3>
        </div>
        <div class="box-widget fl-wrap">
            <div class="box-widget-content">
                <ul class="cat-item no-list-style">
                    @foreach(\App\Models\PostCategory::whereNull('parent_id')->get() as $category)
                    <li><a href="{{$category->url}}">{{$category->title}}</a> <span>{{number_format($category->posts()->count(),0)}}</span></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>                               
</div>