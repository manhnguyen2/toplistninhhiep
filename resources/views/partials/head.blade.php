<head>
    <!--=============== basic  ===============-->
    <meta charset="UTF-8">
    <title>{{env('APP_NAME')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    @yield('meta')
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:site_name" content="{{env('APP_NAME')}}">
    <link rel="canonical" href="{{request()->url()}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;900&family=Roboto:ital,wght@0,300;0,400;0,700;1,400;1,900&display=swap" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/themes/townhub/css/reset.css">
    <link type="text/css" rel="stylesheet" href="/themes/townhub/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="/themes/townhub/css/style.css?v={{date('YmdHis')}}">
    <link type="text/css" rel="stylesheet" href="/themes/townhub/css/color.css?v={{date('YmdHis')}}">
    <link type="text/css" rel="stylesheet" href="/css/toastr.css">
    @section('styles')
    @show
    <!--=============== favicons ===============-->
    <link rel="shortcut icon" href="{{ asset('themes/townhub/images/favicon.ico') }}">
</head>