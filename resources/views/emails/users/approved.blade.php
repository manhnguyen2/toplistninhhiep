<h3>User activated</h3>
<p>You are now free to use our service.</p>
<div>
    <p>Your license will be valid until {{ date('d/m/Y',strtotime($end_at)) }}</p>
</div>