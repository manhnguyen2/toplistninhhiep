@extends('layouts.auth')

@section('content')

<div class="clearfix">
    <div class="box-left">
        <div class="box-outer">
            <div class="box-cell">
                <div class="box-inner">
                    <h2>Bạn chưa có tài khoản? Đăng ký ngay!</h2>
                    <a href="{{ route('login-facebook') }}" class="btn btn-facebook w-100 text-white mb-3"> Đăng nhập với Facebook</a>
            
                    <form method="POST" action="{{ route('auth.register') }}" class="login-form">
                        @csrf
                        <div class="form-group mb-2">
                            <input type="text" class="form-control" name="username" placeholder="Số điện thoại *" required autofocus>
                        </div>
                        <div class="form-group mb-2">
                            <input type="text" class="form-control" name="shop_name" value="" placeholder="Tên shop">
                        </div>
                        <div class="form-group mb-2">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email *" required autocomplete="email" autofocus>
        
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
        
                        <div class="form-group mb-2">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Mật khẩu * " autocomplete="current-password">
        
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mb-2">
                            <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required placeholder="Mật khẩu xác nhận * ">
        
                            @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mb-2" data-otp="true" style="display:none;">
                            <input id="otp" type="text" class="form-control" name="otp" placeholder="Mã OTP (*)">
                        </div>
                        <div class="form-group mb-2">
                            <label class="d-flex align-items-center gap-2">
                                <input type="checkbox" name="check_shop" style="width:20px;height:20px;">
                                <span>Đăng ký cửa hàng</span>
                            </label>
                        </div>
                
                        <div class="row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-theme w-100 btn-primary">Đăng ký</button>
                            </div>
                        </div>
                        <a class="mt-2 btn btn-link" href="{{ route('login') }}">Có tài khoản? Đăng nhập ngay</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="box-right">
        <div class="overlay"></div>
        <div class="bg-cover"></div>
        <div class="box-outer">
            <div class="box-cell">
                <div class="box-inner">
                    <div class="d-flex align-items-center">
                        <div class="textbox-icon">
                            <i style="color: #ffffff" class="et-globe"></i>
                        </div>
                        <h3>Tìm kiếm cửa hàng, doanh nghiệp, công xưởng... uy tín một cách dễ dàng.</h3>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="textbox-icon">
                            <i style="color: #ffffff" class="sl-cup"></i>
                        </div>
                        <h3>Truyền thông thương hiệu miễn phí, tiếp cận khách hàng mới mỗi ngày.</h3>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="textbox-icon">
                            <i style="color: #ffffff" class="et-hazardous"></i>
                        </div>
                        <h3>Cập nhật xu hướng thời trang, nguồn hàng mới nhất từ cửa hàng tại Ninh Hiệp.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(function(){
        $('.login-form').submit(function(e){
            e.preventDefault()
            $.ajax({
                url:'{{ route('auth.register') }}',method:'POST',data:new FormData(this),
                contentType:false,processData:false,
                success:function(resp) {
                    if (resp) {
                        if (resp.sent) {
                            toastr['warning']('Hãy nhập mã OTP để xác nhận số điện thoại')
                            $('[data-otp="true"]').parent().find('.form-group').toggle(false)
                            $('[data-otp="true"]').toggle(true)
                        } else if (resp.sent_failed) {
                            toastr['error']('Mã OTP không chính xác')
                        } else {
                            toastr['success']('Đăng ký thành công')
                            setTimeout(function(){
                                if (resp.redirect == 'shop') {
                                    location.href = '/profile/edit'
                                } else {
                                    location.href = '/home'
                                }
                            }, 300)
                        }
                    }
                },
                error:function(x,h,r) {
                    if (x.status==422 && x.responseJSON && x.responseJSON.errors) {
                        Object.keys(x.responseJSON.errors).forEach(prop => {
                            toastr['error'](x.responseJSON.errors[prop])
                        })
                    }
                }
            })
        })
    })
</script>
@endsection