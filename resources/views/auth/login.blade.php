@extends('layouts.auth')

@section('content')

<div class="clearfix">
    <div class="box-left">
        <div class="box-outer">
            <div class="box-cell">
                <div class="box-inner">
                    <h2>Chào mừng bạn quay trở lại website</h2>
                    <a href="{{ route('login-facebook') }}" class="btn btn-facebook w-100 text-white mb-3"> Đăng nhập với Facebook</a>
            
                    <form method="POST" action="{{ route('login') }}" class="login-form">
                        @csrf
        
                        <div class="form-group mb-2">
                            <input class="form-control" placeholder="Số điện thoại/Email" name="username" required autofocus>
                        </div>
        
                        <div class="form-group mb-2">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="123123" required placeholder="Mật khẩu" autocomplete="current-password">
        
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
        
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <div class="">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        
                                    <label class="form-check-label" for="remember">Ghi nhớ</label>
                                </div>
                            </div>
                            <div class="">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">Quên mật khẩu</a>
                                @endif
                            </div>
                        </div>
        
                        <div class="row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-theme w-100 btn-primary">Đăng nhập</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="box-right">
        <div class="overlay"></div>
        <div class="bg-cover"></div>
        <div class="box-outer">
            <div class="box-cell">
                <div class="box-inner">
                    <div class="d-flex align-items-center">
                        <div class="textbox-icon">
                            <i style="color: #ffffff" class="et-globe"></i>
                        </div>
                        <h3>Tìm kiếm cửa hàng, doanh nghiệp, công xưởng... uy tín một cách dễ dàng.</h3>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="textbox-icon">
                            <i style="color: #ffffff" class="sl-cup"></i>
                        </div>
                        <h3>Truyền thông thương hiệu miễn phí, tiếp cận khách hàng mới mỗi ngày.</h3>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="textbox-icon">
                            <i style="color: #ffffff" class="et-hazardous"></i>
                        </div>
                        <h3>Cập nhật xu hướng thời trang, nguồn hàng mới nhất từ cửa hàng tại Ninh Hiệp.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('.login-form').submit(function(e){
        e.preventDefault()
        $.ajax({
            url:'{{route('auth.login')}}',method:'POST',
            dataType:'json',data:new FormData(this),
            contentType:false,processData:false,
            success:function(resp) {
                if (resp.success) {
                    toastr['info']('Đăng nhập thành công')
                    if (resp.redirect) {
                        setTimeout(function(){
                            location.href = resp.redirect
                        }, 500)
                    }
                } else {
                    toastr['error']('Tài khoản đăng nhập không chính xác')
                }
            }
        })
    })
</script>
@endsection