@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{'Trang cá nhân - ' . env('APP_NAME')}}"/>
<meta name="description" content="{{'Trang cá nhân - ' . env('APP_NAME')}}"/>
<meta property="og:image" content="{{env('APP_LOGO')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{'Trang cá nhân - ' . env('APP_NAME')}}">
<meta property="og:description" content="{{'Trang cá nhân - ' . env('APP_NAME')}}">
@stop
@section('styles')
@include('ckfinder::setup')
@endsection
@section('content')
<link type="text/css" rel="stylesheet" href="/themes/townhub/css/dashboard-style.css">
<form id="form_edit_profile" enctype="multipart/form-data">
    @csrf
    <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
        <div class="container">
            <div class="dashboard-breadcrumbs breadcrumbs"><a href="/">Home</a><span>Chỉnh sửa thông tin cá nhân</span></div>
            <!--Tariff Plan menu-->
            <div class="tfp-btn"><span>Loại tài khoản : </span> <strong>@if ($user->type == \App\Models\User::TYPE_SUPER_ADMIN)Super Admin @elseif ($user->type == \App\Models\User::TYPE_BOOTH_USER) Gian hàng @else Tài khoản cá nhân @endif</strong></div>
            @if ($user->type == \App\Models\User::TYPE_NORMAL_USER)
            <div class="tfp-det">
                <p>Tài khoản của bạn đang là tài khoản <a href="#">cá nhân</a> . Click link bên dưới để nâng cấp lên tài khoản gian hàng </p>
                <a href="#" class="tfp-det-btn color2-bg">Xem chi tiết</a>
            </div>
            @endif
            <!--Tariff Plan menu end-->
            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                <h1>Chào mừng  : <span>{{ $user->name }}</span></h1>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="dashboard-header fl-wrap">
            <div class="container">
                <div class="dashboard-header_conatiner fl-wrap">
                    <div class="dashboard-header-avatar">
                        <img src="{{ asset($user->avatar) }}" alt="{{ $user->name }}">
                    </div>
                    @if(0)
                    <div class="dashboard-header-stats-wrap">
                        <div class="dashboard-header-stats">
                            <div class="swiper-container swiper-container-horizontal" style="cursor: grab;">
                                <div class="swiper-wrapper">
                                    <!--  dashboard-header-stats-item -->
                                    <div class="swiper-slide swiper-slide-active" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-map-marked"></i>
                                            Active Listings
                                            <span>21</span>
                                        </div>
                                    </div>
                                    <!--  dashboard-header-stats-item end -->
                                    <!--  dashboard-header-stats-item -->
                                    <div class="swiper-slide swiper-slide-next" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-chart-bar"></i>
                                            Listing Views
                                            <span>1054</span>
                                        </div>
                                    </div>
                                    <!--  dashboard-header-stats-item end -->
                                    <!--  dashboard-header-stats-item -->
                                    <div class="swiper-slide" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-comments-alt"></i>
                                            Total Reviews
                                            <span>79</span>
                                        </div>
                                    </div>
                                    <!--  dashboard-header-stats-item end -->
                                    <!--  dashboard-header-stats-item -->
                                    <div class="swiper-slide" style="width: 180px; margin-right: 10px;">
                                        <div class="dashboard-header-stats-item">
                                            <i class="fal fa-heart"></i>
                                            Times Bookmarked
                                            <span>654</span>
                                        </div>
                                    </div>
                                    <!--  dashboard-header-stats-item end -->
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                        </div>
                        <!--  dashboard-header-stats  end -->
                        <div class="dhs-controls">
                            <div class="dhs dhs-prev swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"><i class="fal fa-angle-left"></i></div>
                            <div class="dhs dhs-next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"><i class="fal fa-angle-right"></i></div>
                        </div>
                    </div>
                    <!--  dashboard-header-stats-wrap end -->
                    <a class="add_new-dashboard">Add Listing <i class="fal fa-layer-plus"></i></a>
                    @endif
                </div>
            </div>
        </div>
        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
        <div class="circle-wrap" style="left: 120px; bottom: 120px; transform: translateZ(0px) translateY(35.3982px);" data-scrollax="properties: { translateY: '-200px' }">
            <div class="circle_bg-bal circle_bg-bal_small"></div>
        </div>
        <div class="circle-wrap" style="right: 420px; bottom: -70px; transform: translateZ(0px) translateY(-26.5487px);" data-scrollax="properties: { translateY: '150px' }">
            <div class="circle_bg-bal circle_bg-bal_big"></div>
        </div>
        <div class="circle-wrap" style="left: 420px; top: -70px; transform: translateZ(0px) translateY(-17.6991px);" data-scrollax="properties: { translateY: '100px' }">
            <div class="circle_bg-bal circle_bg-bal_big"></div>
        </div>
        <div class="circle-wrap" style="left:40%;bottom:-70px;">
            <div class="circle_bg-bal circle_bg-bal_middle"></div>
        </div>
        <div class="circle-wrap" style="right:40%;top:-10px;">
            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.9469px);"></div>
        </div>
        <div class="circle-wrap" style="right:55%;top:90px;">
            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }" style="transform: translateZ(0px) translateY(61.9469px);"></div>
        </div>
    </section>
    <section class="gray-bg main-dashboard-sec" id="sec1">
        <div class="container">
            <div class="row">
            <!--  dashboard-menu-->
            <div class="col-md-3">
                <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> Dashboard menu</div>
                <div class="clearfix"></div>
                <div class="fixed-bar fl-wrap" id="dash_menu">
                    <div class="user-profile-menu-wrap fl-wrap block_box">
                        <!-- user-profile-menu-->
                        <div class="user-profile-menu">
                            <h3>Menu</h3>
                            <ul class="no-list-style">
                                <li><a href="{{ route('profile') }}"><i class="fal fa-user"></i>Thông tin cá nhân</a></li>
                            </ul>
                        </div>
                        <!-- user-profile-menu end-->
                    
                        <a href="{{ route('logout') }}" class="logout_btn color2-bg">Đăng xuất <i class="fas fa-sign-out"></i></a>
                    </div>
                </div>
            <div class="clearfix"></div>
            </div>
            <!-- dashboard-menu  end-->
            <!-- dashboard content-->
            <div class="col-md-9">
                <div class="dashboard-title fl-wrap">
                    <h3>Thông tin cá nhân</h3>
                </div>
                <!-- profile-edit-container-->
                <div class="profile-edit-container fl-wrap block_box">
                    <div class="custom-form">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Họ và tên <i class="fal fa-user"></i></label>
                                <input type="text" placeholder="Họ và tên" name="name" value="{{ $user->name }}">
                            </div>
                            <div class="col-sm-4">
                                <label>Email <i class="far fa-envelope"></i>  </label>
                                <input type="text" placeholder="Email" name="email" value="{{ $user->email }}">
                            </div>
                            <div class="col-sm-4">
                                <label>Số điện thoại <i class="fal fa-phone"></i></label>
                                <input type="text" placeholder="Số điện thoại" readonly value="{{ $user->username }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Ảnh đại diện</label>
                                <input type="file" onchange="loadFile(event)" class="d-none" name="file_upload" accept="image/*">
                                <img class="img-selector" id="output" src="{{$user->avatar?:'/img/no-image.jpg'}}" style="height:100px;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn    color2-bg  float-btn">Cập nhật thông tin cá nhân<i class="fal fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- profile-edit-container end-->
                @if($user->type != 3 && 1==0)
                <div class="dashboard-title dt-inbox fl-wrap">
                    <h3>Gian hàng</h3>
                </div>
                <!-- profile-edit-container-->
                <div class="profile-edit-container fl-wrap block_box">
                    <div class="custom-form">
                    <div class="row">
                            <div class="col-sm-6">
                                <label>Tên gian hàng</label>
                                <input type="text" placeholder="" name="name" value="{{ $user->store->title }}">
                            </div>
                            <div class="col-sm-6">
                                <label>Danh mục gian hàng <i class="far fa-envelope"></i>  </label>
                                <input type="text" placeholder="JessieManrty@domain.com" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <label>Đổi ảnh đại diện</label> 
                        <div class="clearfix"></div>
                        <div class="listsearch-input-item fl-wrap">
                        <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Avatar:</label>
                                    <div class="col-lg-9">
                                        <img class="img-responsive img-fluid" id="img-avatar" src="{{ asset($user->avatar) }}">
                                        <label class="custom-file">
                                                <input type="hidden"  id="url-avatar" class="custom-file-input" value=" {{ $user->avatar }}" name="avatar">
                                                <a id="avatar" onclick="selectFileWithCKFinder()" class="custom-file-label color2-bg logout_btn">Chọn ảnh</a>
                                        </label>
                                        <button class="btn  color2-bg  float-btn">Cập nhật thông tin cá nhân<i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                @endif
                <!-- profile-edit-container end-->
            </div>
            <!-- dashboard content end-->
            </div>
        </div>
    </section>
</form>
<style>
    .img-selector {
        height:100px;
        cursor: pointer;
    }
</style>
@endsection
@section('scripts')
<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
        }
    };
    $(function(){
        $('.img-selector').click(function(){
            $('[type=file]').trigger('click')
        })
        $('#form_edit_profile').submit(function(e){
            e.preventDefault()
            $.ajax({
                url:'{{route('profile')}}',method:'POST',
                data:new FormData(this),
                contentType:false,processData:false,
                success:function(resp) {
                    if (resp.success) {
                        toastr['info']('Cập nhật thành công')
                    } else {
                        toastr['error']('Cập nhật thất bại')
                    }
                },error:function(x,h,r) {
                    toastr['error']('Cập nhật thất bại')
                }
            })
        })
    })
</script>
@endsection