@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{$record->title}}"/>
<meta name="description" content="{{$record->description}}"/>
<meta property="og:image" content="{{env('APP_URL').$record->image}}">
<meta property="og:type" content="article">
<meta property="og:title" content="{{$record->title}}">
<meta property="og:description" content="{{$record->description}}">
@stop
@section('content')
<!--  section  -->
<section class="parallax-section single-par" data-scrollax-parent="true">
    <div class="bg par-elem "  data-bg="{{$record->image?:'/themes/townhub/images/bg/5.jpg'}}" data-scrollax="properties: { translateY: '30%' }"></div>
    <div class="overlay op7"></div>
    <div class="container">
        <div class="section-title center-align big-title">
            <h2><span>Danh mục: {{$record->title}}</span></h2>
            <span class="section-separator"></span>
            <h4>{{$record->description}}</h4>
        </div>
    </div>
    <div class="header-sec-link">
        <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
    </div>
</section>
<!--  section  end-->
<section class="gray-bg no-top-padding-sec" id="sec1">
    <div class="container">
        <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
            <a href="/">Trang chủ</a><span>Tin tức</span> 
        </div>
        <div class="share-holder hid-share sing-page-share top_sing-page-share">
            <div class="share-container  isShare"></div>
        </div>
        <div class="post-container fl-wrap">
            <div class="row">
                <div class="col-md-8">
                    @foreach($posts as $post) 
                    <article class="post-article">
                        <div class="list-single-main-media fl-wrap">
                            <img src="{{$post->image?:'/themes/townhub/images/all/29.jpg'}}" class="respimg" alt="{{$post->title}}">
                        </div>
                        <div class="list-single-main-item fl-wrap block_box">
                            <h2 class="post-opt-title"><a href="{{$post->url}}">{{$post->title}}</a></h2>
                            <p>{{$post->description}}</p>
                            <span class="fw-separator"></span>
                            <div class="post-author"><a href="{{$post->user->name??'anonymous'}}"><img src="{{$post->user->avatar??'/themes/townhub/images/avatar/5.jpg'}}" alt="{{$post->user->name??'anonymous'}}"><span>Đăng bởi , {{$post->user->name??'anonymous'}}</span></a></div>
                            <div class="post-opt">
                                <ul class="no-list-style">
                                    <li><i class="fal fa-calendar"></i> <span>{{date('d/m/Y',strtotime($post->created_at))}}</span></li>
                                    <li><i class="fal fa-eye"></i> <span>{{number_format($post->viewed,0)}}</span></li>
                                    <li><i class="fal fa-tags"></i> <a href="javascript:;">{{$post->category->name??''}}</a> </li>
                                </ul>
                            </div>
                            <a href="{{$post->url}}" class="btn  color2-bg float-btn">Xem chi tiết<i class="fal fa-angle-right"></i></a>
                        </div>
                    </article>
                    @endforeach                                       
                    <div class="pagination">
                        {{$posts->appends(request()->all())->links()}}
                    </div>
                </div>
                <div class="col-md-4">
                    @include('partials.sidebar')
                </div>
                <!-- blog sidebar end -->
            </div>
        </div>
    </div>
</section>
<div class="limit-box fl-wrap"></div>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Post index ready')
    })
</script>
@endsection