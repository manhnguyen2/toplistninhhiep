@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{$post->title}}"/>
<meta name="description" content="{{$post->description}}"/>
<meta property="og:image" content="{{env('APP_URL').$post->image}}">
<meta property="og:type" content="article">
<meta property="og:title" content="{{$post->title}}">
<meta property="og:description" content="{{$post->description}}">
@stop
@section('content')
<div class="content">
    <section class="gray-bg no-top-padding-sec" id="sec1">
        <div class="container">
            <div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
                <a href="/">Trang chủ</a><a href="{{$post->category->url}}">{{$post->category->title}}</a> <span>{{$post->title}}</span> 
                @if(0)
                <div  class="showshare brd-show-share color2-bg"> <i class="fas fa-share"></i> Share </div>
                @endif
            </div>
            <div class="share-holder hid-share sing-page-share top_sing-page-share">
                <div class="share-container  isShare"></div>
            </div>
            <div class="post-container fl-wrap">
                <div class="row">
                    <!-- blog content-->
                    <div class="col-md-8">
                        <!-- article> --> 
                        <article class="post-article single-post-article">
                            <div class="list-single-main-media fl-wrap">
                                <img src="{{$post->image?:'/themes/townhub/images/all/29.jpg'}}" class="respimg" alt="{{$post->title}}">
                            </div>
                            <div class="list-single-main-item fl-wrap block_box">
                                <h2 class="post-opt-title"><a href="{{$post->url}}">{{$post->title}}</a></h2>
                                <div class="post-author"><a href="{{$post->user->name??'anonymous'}}"><img src="{{$post->user->avatar??'/themes/townhub/images/avatar/5.jpg'}}" alt="{{$post->user->name??'anonymous'}}"><span>Đăng bởi, {{$post->user->name??'anonymous'}}</span></a></div>
                                <div class="post-opt">
                                    <ul class="no-list-style">
                                        <li><i class="fal fa-calendar"></i> <span>{{date('d/m/Y',strtotime($post->created_at))}}</span></li>
                                        <li><i class="fal fa-eye"></i> <span>{{number_format($post->viewed,0)}}</span></li>
                                        <li><i class="fal fa-tags"></i> <a href="javascript:;">{{$post->category->name??''}}</a> </li>
                                    </ul>
                                </div>
                                <span class="fw-separator"></span> 
                                <div class="clearfix"></div>
                                <div>
                                {!!$post->content!!}
                                </div>
                                <span class="fw-separator"></span> 
                            </div>
                        </article>                                             
                    </div>
                    <div class="col-md-4">
                        @include('partials.sidebar')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
</div>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Post index ready')
    })
</script>
@endsection