@extends('layouts.app')
@section('meta')
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<meta property="og:image" content="{{env('APP_LOGO')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{env('APP_NAME')}}">
<meta property="og:description" content="{{env('APP_NAME')}}">
@stop
@section('content')

<section   class="gray-bg hidden-section particles-wrapper">
    <div class="container">
        <div class="section-title">
            <h2>Danh mục</h2>
            <div class="section-subtitle">Tất cả</div>
            <span class="section-separator"></span>
        </div>
    </div>
                            
</section>
<section>
    <div class="container big-container">
        <div class="section-title">
            <h2><span>Danh mục cửa hàng</span></h2>
            <div class="section-subtitle">Danh mục cửa hàng</div>
            <span class="section-separator"></span>
        </div>
        <div class="grid-item-holder gallery-items fl-wrap">
            @include('category.stores',['stores'=>$records])
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Category all ready')
    })
</script>
@endsection