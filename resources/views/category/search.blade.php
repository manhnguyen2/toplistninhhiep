@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{$keyword.' - '.env('APP_NAME')}}"/>
<meta name="description" content="{{$keyword.' - '.env('APP_NAME')}}"/>
<meta property="og:image" content="{{env('APP_LOGO')}}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{$keyword.' - '.env('APP_NAME')}}">
<meta property="og:description" content="{{$keyword.' - '.env('APP_NAME')}}">
@stop
@section('content')

<section   class="gray-bg hidden-section particles-wrapper">
    <div class="container">
        <div class="section-title">
            <h2>{{$keyword}}</h2>
            <div class="section-subtitle">Từ khóa</div>
            <span class="section-separator"></span>
        </div>
    </div>
                            
</section>
<section>
    <div class="container big-container">
        <div class="section-title">
            <h2><span>Kết quả tìm kiếm</span></h2>
            <div class="section-subtitle">Kết quả tìm kiếm</div>
            <span class="section-separator"></span>
        </div>
        <div class="grid-item-holder gallery-items fl-wrap">
            @include('category.stores',['stores'=>$records])
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Category search ready')
    })
</script>
@endsection