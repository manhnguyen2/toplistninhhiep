@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{$record->title}}"/>
<meta name="description" content="{{$record->description}}"/>
<meta property="og:image" content="{{env('APP_URL').$record->image}}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{$record->title}}">
<meta property="og:description" content="{{$record->description}}">
@stop
@section('content')

<section   class="gray-bg hidden-section particles-wrapper">
    <div class="container">
        <div class="section-title">
            <h2>{{$record->title}}</h2>
            <div class="section-subtitle">{{$record->title}}</div>
            <span class="section-separator"></span>
            <p>{{$record->description}}</p>
        </div>
        <div class="listing-item-grid_container fl-wrap">
            <div class="row">
                @foreach($record->children as $category)
                <div class="col-sm-4">
                    <div class="listing-item-grid">
                        <div class="bg"  data-bg="{{$category->image?:'/themes/townhub/images/all/56.jpg'}}"></div>
                        <div class="d-gr-sec"></div>
                        <div class="listing-counter color2-bg"><span>{{number_format($category->stores()->count(),0)}}</span> Listings</div>
                        <div class="listing-item-grid_title">
                            <h3><a href="{{$category->url}}">{{$category->title}}</a></h3>
                            <p>{{$category->description}}</p>
                        </div>
                    </div>
                </div>
                @endforeach                                                                                               
            </div>
        </div>

    </div>
                            
</section>
<section>
    <div class="container big-container">
        <div class="section-title">
            <h2><span>Popular Listings</span></h2>
            <div class="section-subtitle">Popular Listings</div>
            <span class="section-separator"></span>
        </div>
        <div class="grid-item-holder gallery-items fl-wrap">
            @include('shared.stores',['stores'=>$record->stores()])
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Category index ready')
    })
</script>
@endsection