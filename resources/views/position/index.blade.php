@extends('layouts.app')
@section('meta')
<meta name="keywords" content="{{$record->title}}"/>
<meta name="description" content="{{$record->title}}"/>
<meta property="og:image" content="{{env('APP_URL').$record->image}}">
<meta property="og:type" content="website">
<meta property="og:title" content="{{$record->title}}">
<meta property="og:description" content="{{$record->title}}">
@stop
@section('content')
<section>
    <div class="container big-container">
        <div class="section-title">
            <h2><span>{{$record->title}}</span></h2>
            <div class="section-subtitle">{{$record->title}}</div>
            <span class="section-separator"></span>
        </div>
        <div class="grid-item-holder gallery-items fl-wrap">
            @include('position.stores')
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $(function(){
        console.log('Position index ready')
    })
</script>
@endsection