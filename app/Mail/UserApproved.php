<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserApproved extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $end_at;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($end_at)
    {
        $this->end_at = $end_at;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.users.approved');
    }
}
