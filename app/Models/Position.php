<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function stores() {
        return $this->hasMany(Store::class,'position_id');
    }
    public function getUrlAttribute() {
        return '/dia-diem/'.$this->slug;
    }
}
