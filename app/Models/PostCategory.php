<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'title', 'image', 'description', 'status','parent_id','slug'];
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function parent() {
        return $this->belongsTo(Category::class);
    }
    public function posts() {
        $id = $this->id;
        $ids = PostCategory::where(function($p)use($id){
            $p->where('id', $id)->orWhere('parent_id', $id);
        })->pluck('id');
        return Post::whereIn('category_id', $ids);
    }
    public function children() {
        return $this->hasMany(Category::class,'parent_id');
    }
    public function getUrlAttribute() {
        return '/tin-tuc/'.$this->slug;
    }
}
