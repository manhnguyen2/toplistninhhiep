<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'title', 'image', 'description', 'content', 'category_id', 'status', 'slug','viewed'];
    public function category ()
    {
        return $this->belongsTo(PostCategory::class);
    }
    public function user ()
    {
        return $this->belongsTo(User::class);
    }
    public function getUrlAttribute() {
        return '/'.$this->slug;
    }
}
