<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'title', 'image', 'description', 'status','parent_id','slug'];
    public function stores() {
        if (in_array($this->slug,['thoi-trang'])) {
            return Store::where('type_id', 2)->get();
        }
        if (in_array($this->slug,['xuong'])) {
            return Store::where('type_id', 3)->get();
        }
        if (in_array($this->slug,['nguyen-lieu'])) {
            return Store::where('type_id', 1)->get();
        }
        $id = $this->id;
        $ids = Category::where(function($p)use($id){
            $p->where('id', $id)->orWhere('parent_id', $id);
        })->pluck('id');
        return Store::whereIn('id', CategoryStore::whereIn('category_id', $ids)->pluck('store_id'))->get();
    }
    public function parent() {
        return $this->belongsTo(Category::class);
    }
    public function children() {
        return $this->hasMany(Category::class,'parent_id');
    }
    public function getUrlAttribute() {
        return '/danh-muc/'.$this->slug;
    }
}
