<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function type()
    {
        return $this->belongsTo(Type::class);
    }
    public function getBannerAttribute($value)
    {
        return json_decode($value, true) ?: [];
    }

    public function setBannerAttribute($value)
    {
        $this->attributes['banner'] = $value ? json_encode($value) : '';
    }
    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    public function position()
    {
        return $this->belongsTo(Position::class);
    }
    public function categories()
    {
        return $this->hasManyThrough(Category::class,CategoryStore::class,'store_id','id','id','category_id');
    }
    public function getUrlAttribute() {
        return '/cua-hang/'.$this->slug;
    }
}
