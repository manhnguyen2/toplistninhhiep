<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules(Request $request)
    {
        if ($request->otp) {
            return [];
        }
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'username' => 'required|unique:users',
        ];
    }
    public function messages()
    {
        return [
            'username.unique' => 'Tên đăng nhập/SĐT đã tồn tại',
            'email.unique' => 'Email đã được sử dụng',
        ];
    }
}
