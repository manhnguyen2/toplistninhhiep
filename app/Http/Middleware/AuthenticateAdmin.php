<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;

class AuthenticateAdmin
{
    
    public function handle($request, Closure $next)
    {
        if ($request->user() && ($request->user()->type == \App\Models\User::TYPE_SUPER_ADMIN || $request->user()->type == \App\Models\User::TYPE_EDITOR)) {
            return $next($request);
        }
        
        return redirect('login');
    }
}
