<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Store;

class ProvinceController extends Controller
{
    public function index($alias, Request $request) {
        $record = Province::where('slug',$alias)->firstOrFail();
        return view('province.index',compact('record'));
    }
}
