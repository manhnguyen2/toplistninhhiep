<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Store;

class CategoryController extends Controller
{
    public function index($alias, Request $request) {
        $record = Category::where('slug',$alias)->firstOrFail();
        return view('category.index',compact('record'));
    }
    public function search(Request $request) {
        $query = Store::latest();
        if ($request->keyword) {
            $query->where(function($p)use($request){
                $p->where('title','like','%'.$request->keyword.'%')
                ->orWhere('slug','like','%'.$request->keyword.'%')
                ->orWhere('description','like','%'.$request->keyword.'%')
                ->orWhere('content','like','%'.$request->keyword.'%');
            });
        }
        if ($request->type) {
            $query->where('type_id',$request->type);
        }
        if ($request->position) {
            $query->where('position_id',$request->position);
        }
        $records = $query->paginate(20);
        $keyword = $request->keyword;
        return view('category.search',compact('records','keyword'));
    }
    public function all(Request $request) {
        $query = Store::latest();
        $records = $query->paginate(20);
        return view('category.all',compact('records'));
    }
}
