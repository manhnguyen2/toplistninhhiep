<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Position;

class PositionController extends Controller
{
    public function index($alias, Request $request) {
        $record = Position::where('slug',$alias)->firstOrFail();
        return view('position.index',compact('record'));
    }
}
