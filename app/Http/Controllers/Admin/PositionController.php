<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.position.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $request->merge([
            'slug'=>Str::slug($request->title),
            'image' => $request->file_upload ? ('/storage/'. $request->file_upload->store('userfiles/images', 'public')) : $category->image
        ]);
        $input = $request->except(['_token','file_upload']);
        $position =  Position::create($input);
        return redirect()->route('admin.position.show')->with('success', 'Thêm vị trí thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        //
        $positions = Position::all();
        return view('admin.position.show', ['positions' => $positions ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Position::find($id);
        return view('admin.position.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Position $position)
    {

        $request->merge([
            'slug'=>Str::slug($request->title),
            'image' => $request->file_upload ? ('/storage/'. $request->file_upload->store('userfiles/images', 'public')) : $category->image
        ]);
        $input = $request->except(['_token', 'id','file_upload']);
        $position = Position::find($request->id);
        if ($position->update($input))
        {
            return redirect()->route('admin.position.show')->with('success', 'Sửa vị trí thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $position = Position::find($id);
        if ($position->delete())
        {
            return redirect()->route('admin.position.show')->with('success', 'Xóa vị trí thành công');
        }
    }
}
