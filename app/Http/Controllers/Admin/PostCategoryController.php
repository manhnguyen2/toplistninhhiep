<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;

class PostCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.postcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'slug' => Str::slug($request->title),
            'image' => $request->file_upload ? ('/storage/'. $request->file_upload->store('userfiles/images', 'public')) : $category->image
        ]);
        $input = $request->except(['_token','file_upload']);
        $postcategory =  PostCategory::create($input);
        return redirect()->route('admin.postcategory.show')->with('success', 'Thêm danh mục bài viết thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function show(PostCategory $postCategory)
    {
        //
        $categories = PostCategory::all();
        return view('admin.postcategory.show',['categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = PostCategory::find($id);
        return view('admin.postcategory.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->merge([
            'slug' => Str::slug($request->title),
            'image' => $request->file_upload ? ('/storage/'. $request->file_upload->store('userfiles/images', 'public')) : $category->image
        ]);
        $input = $request->except(['_token', 'id','file_upload']);
        $postcategory = PostCategory::find($request->id);
        $postcategory->updated_at = Carbon::now();
        if ($postcategory->update($input))
        {
            return redirect()->route('admin.postcategory.show')->with('success', 'Sửa danh mục bài viết thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // 
        $postcategory = PostCategory::find($id);
        if ($postcategory->delete())
        {
            return redirect()->route('admin.postcategory.show')->with('success', 'Xóa bài viết thành công');
        }
    }
}
