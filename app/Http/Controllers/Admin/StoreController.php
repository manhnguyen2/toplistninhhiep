<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\Type;
use App\Models\User;
use App\Models\Category;
use App\Models\CategoryStore;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create()
    {
        $types = Type::all();
        $categories = Category::all();
        $positions = Position::all();
        return view('admin.store.create',compact('types','categories', 'positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file_banner_upload) {
            $request->merge([
                'banner' => [$request->file_banner_upload ? ('/storage/'. $request->file_banner_upload->store('userfiles/images', 'public')) : null]
            ]);
        } else {
            $request->merge(['banner'=>[]]);
        }
        $request->merge([
            'slug' => Str::slug($request->title),
            'avatar' => $request->file_upload ? ('/storage/'. $request->file_upload->store('userfiles/images', 'public')) : null,
        ]);
        $input = $request->except(['_token','category_id','file_upload','file_banner_upload']);
        $input['user_id'] = auth()->user()->id;
        $record = Store::create($input);
        foreach($request->category_id ?: [] as $category_id) {
            CategoryStore::insert(['store_id'=>$record->id,'category_id'=>$category_id]);
        }
        return redirect()->route('admin.store.show')->with('success', 'Thêm gian hàng thành công');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
        $data = Store::All();
        return view('admin/store/show', ['stores' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Store::find($id);
        $types = Type::all();
        $categories = Category::all();
        $positions = Position::all();
        return view('admin/store/edit', ['data' => $data, 'id' => $id, 'types' => $types, 'categories'=> $categories,'positions'=>$positions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        if ($request->file_banner_upload) {
            $request->merge([
                'banner' => [$request->file_banner_upload ? ('/storage/'. $request->file_banner_upload->store('userfiles/images', 'public')) : null]
            ]);
        }
        $request->merge([
            'slug' => Str::slug($request->title),
        ]);
        $input = $request->except(['_token', 'id','category_id','file_upload','file_banner_upload']);
        $store = Store::find($request->id);
        CategoryStore::where('store_id',$request->id)->delete();
        foreach($request->category_id ?: [] as $category_id) {
            CategoryStore::updateOrCreate([
                'category_id' => $category_id,
                'store_id' => $request->id,
            ]);
        }
        if ($store->update($input))
        {
            return back()->with('success', 'Cập nhật gian hàng thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }
    public function approve($id)
    {
        $data = Store::find($id);
        $data->user->status = 1;
        if ($data->user->save())
        {
            return back()->with('success', 'Duyệt thành viên thành công!');
        }

    }
}
