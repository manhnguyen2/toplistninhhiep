<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'slug' => Str::slug($request->title),
            'image' => '/storage/'. $request->file_upload->store('userfiles/images', 'public')
        ]);
        $input = $request->except(['_token']);
        $category =  Category::create($input);
        return redirect()->route('admin.category.show')->with('success', 'Thêm danh mục cửa hàng thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
        $categories = Category::all();
        return view('admin.category.show',['categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Category::find($id);
        return view('admin.category.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category = Category::find($request->id);
        $request->merge([
            'slug' => Str::slug($request->title),
            'image' => $request->file_upload ? ('/storage/'. $request->file_upload->store('userfiles/images', 'public')) : $category->image
        ]);
        $input = $request->except(['_token', 'id']);
        if ($category->update($input))
        {
            return redirect()->route('admin.category.show')->with('success', 'Sửa danh mục cửa hàng thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);
        if ($category->delete())
        {
            return redirect()->route('admin.category.show')->with('success', 'Xóa bài viết thành công');
        }
    }
}
