<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\User;
use App\Models\Post;

class DashboardController extends Controller
{
    public function index()
    {
        $store = Store::orderBy('created_at', 'desc')->limit(10)->get();
        $posts = Post::orderBy('created_at', 'desc')->limit(10)->get();
        $count_store = Store::count();
        $count_user = User::count();
        $count_post = Post::count();
        return view('admin.dashboard.index', ['store'=>$store,'posts'=>$posts, 'count_store' => $count_store, 'count_post'=> $count_post, 'count_user'=> $count_user]);
    }
    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }
}