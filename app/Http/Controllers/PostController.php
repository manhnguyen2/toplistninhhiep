<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\PostCategory;

class PostController extends Controller
{
    public function index($alias, Request $request) {
        $record = PostCategory::where('slug',$alias)->firstOrFail();
        if (!$record) {
            $record = PostCategory::first();
        }
        $query = $record->posts()->latest();
        if ($request->se) {
            $query->where(function($sub)use($request){
                return $sub->where('title','like','%'.$request->se.'%')
                ->orWhere('description','like','%'.$request->se.'%')
                ->orWhere('content','like','%'.$request->se.'%');
            });
        }
        $posts = $query->paginate(30);
        return view('post.index',compact('record','posts'));
    }
    public function detail($alias, Request $request) {
        $post = Post::where('slug',$alias)->firstOrFail();
        return view('post.detail',compact('post'));
    }
}
