<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Store;

class StoreController extends Controller
{
    public function detail($alias, Request $request) {
        $record = Store::where('slug',$alias)->firstOrFail();
        return view('store.detail',compact('record'));
    }
}
