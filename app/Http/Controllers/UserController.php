<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function subscribe(Request $request)
    {
        Subscriber::updateOrCreate([
            'email' => $request->email
        ],[]);
        return response()->json(['success'=>true]);
    }
    public function profile(Request $request)
    {
        $user = Auth::user();
        if (request()->ajax()) {
            $image = null;
            if ($request->file_upload) {
                $image = '/storage/'. $request->file_upload->store('userfiles/images', 'public');
            }
            request()->merge([
                'avatar' => $image ?: $user->avatar
            ]);
            $user->update([
                'name' => request('name'),
                'email' => request('email'),
                'avatar' => request('avatar'),
            ]);
            return response()->json(['success'=>true]);
        }
        return view('profile.index', ['user'=>$user]);
    }
}
