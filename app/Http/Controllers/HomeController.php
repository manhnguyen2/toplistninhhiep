<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Position;
use App\Models\Store;

class HomeController extends Controller
{
    public function index()
    {
        $positions = Position::all();
        $stores = [];
        foreach(['Nguyên liệu','Thời trang','Xưởng'] as $key=>$val) {
            $stores[$val] = Store::where('type_id' , $key+1)->limit(4)->get();
        }
        return view('home.index', compact('positions','stores'));
    }
    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }
}
