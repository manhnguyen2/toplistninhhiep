<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username')->unique();
            $table->string('avatar')->nullable()->default(null);
            $table->string('facebook_id')->nullable()->default(null);
            $table->text('facebook_token')->nullable()->default(null);
            $table->text('facebook_refresh_token')->nullable()->default(null);
            $table->string('email')->unique();
            $table->string('code')->nullable()->default(null);
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->datetime('start_at')->nullable()->default(null);
            $table->datetime('end_at')->nullable()->default(null);
            $table->string('password');
            $table->integer('type')->default(3);
            $table->integer('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
