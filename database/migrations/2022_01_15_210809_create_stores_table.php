<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('description')->nullalble();
            $table->string('content')->nullalble();
            $table->string('avatar')->nullable();
            $table->text('address')->nullable();
            $table->string('banner')->nullable();
            $table->string('phone')->nullable();
            $table->unsignedInteger('province_id')->nullable();
            $table->unsignedInteger('type_id')->nullable();
            $table->unsignedInteger('position_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
