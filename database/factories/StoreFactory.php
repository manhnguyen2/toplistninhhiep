<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StoreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'title' => $this->faker->name(),
            'avatar' => '/assets/img/store-sample.jpg',
            'banner' => '/assets/img/banner-sample.jpg',
            'created_at' => now(),
            'updated_at' => now(),
            'type_id'=> rand(1,3),
            'description'=> $this->faker->text(100),
            'phone' => $this->faker->phoneNumber(),
        ];
    }
}
