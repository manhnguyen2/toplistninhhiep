<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = 'Một nữ thần tuổi trung niên nhất định phải có vài món đồ, có thể khiến bạn trẻ ra';
        return [
            //
            'title' => $title,
            'slug' => str_slug($title),
            'content' => $this->faker->text(300),
            'description' => $this->faker->text(200),
            'category_id' => rand(1,10),
            'image' => '/assets/img/post-sample.jpg'
        ];
    }
}
