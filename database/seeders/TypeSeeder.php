<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('types')->insert([
            'title' => 'Nguyên liệu'
        ]);
        DB::table('types')->insert([
            'title' => 'Thời trang'
        ]);
        DB::table('types')->insert([
            'title' => 'Xưởng'
        ]);
    }
}
