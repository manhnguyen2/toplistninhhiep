<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/tim-kiem', [App\Http\Controllers\CategoryController::class, 'search'])->name('search');
Route::get('/danh-muc', [App\Http\Controllers\CategoryController::class, 'all'])->name('category.all');
Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');
Route::post('/auth/register', [App\Http\Controllers\AuthController::class, 'register'])->name('auth.register');
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');
Route::prefix('admin')->middleware(['auth.admin'])->group(function() {
    Route::get('/', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('/logout', [App\Http\Controllers\Admin\DashboardController::class, 'logout'])->name('admin.logout');
    // store
    Route::get('/store/create', [App\Http\Controllers\Admin\StoreController::class, 'create'])->name('admin.store.create');
    Route::post('/store/store', [App\Http\Controllers\Admin\StoreController::class, 'store'])->name('admin.store.store');
    Route::get('/store/edit/{id}', [App\Http\Controllers\Admin\StoreController::class, 'edit'])->name('admin.store.edit');
    Route::post('/store/update', [App\Http\Controllers\Admin\StoreController::class, 'update'])->name('admin.store.update');
    Route::get('/store/approve/{id}', [App\Http\Controllers\Admin\StoreController::class, 'approve'])->name('admin.store.approve');
    Route::get('/store/show', [App\Http\Controllers\Admin\StoreController::class, 'show'])->name('admin.store.show');
    // user
    Route::get('/user', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('admin.user.index');
    Route::post('/user/update', [App\Http\Controllers\Admin\UserController::class, 'show'])->name('admin.user.update');
    Route::get('/user/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'edit'])->name('admin.user.edit');
    Route::get('/user/approve/{id}', [App\Http\Controllers\Admin\UserController::class, 'approve'])->name('admin.user.approve');
    //post
    Route::get('/post/show', [App\Http\Controllers\Admin\PostController::class, 'show'])->name('admin.post.show');
    Route::post('/post/update', [App\Http\Controllers\Admin\PostController::class, 'update'])->name('admin.post.update');
    Route::get('/post/edit/{id}', [App\Http\Controllers\Admin\PostController::class, 'edit'])->name('admin.post.edit');
    Route::get('/post/store', [App\Http\Controllers\Admin\PostController::class, 'create'])->name('admin.post.store');
    Route::post('/post/store', [App\Http\Controllers\Admin\PostController::class, 'store'])->name('admin.post.store');
    Route::get('/post/destroy/{id}', [App\Http\Controllers\Admin\PostController::class, 'destroy'])->name('admin.post.destroy');
    //postCategory
    Route::get('/postcategory/show', [App\Http\Controllers\Admin\PostCategoryController::class, 'show'])->name('admin.postcategory.show');
    Route::post('/postcategory/update', [App\Http\Controllers\Admin\PostCategoryController::class, 'update'])->name('admin.postcategory.update');
    Route::get('/postcategory/edit/{id}', [App\Http\Controllers\Admin\PostCategoryController::class, 'edit'])->name('admin.postcategory.edit');
    Route::get('/postcategory/store', [App\Http\Controllers\Admin\PostCategoryController::class, 'create'])->name('admin.postcategory.store');
    Route::post('/postcategory/store', [App\Http\Controllers\Admin\PostCategoryController::class, 'store'])->name('admin.postcategory.store');
    Route::get('/postcategory/destroy/{id}', [App\Http\Controllers\Admin\PostCategoryController::class, 'destroy'])->name('admin.postcategory.destroy');
    //Category
    Route::get('/category/show', [App\Http\Controllers\Admin\CategoryController::class, 'show'])->name('admin.category.show');
    Route::post('/category/update', [App\Http\Controllers\Admin\CategoryController::class, 'update'])->name('admin.category.update');
    Route::get('/category/edit/{id}', [App\Http\Controllers\Admin\CategoryController::class, 'edit'])->name('admin.category.edit');
    Route::get('/category/store', [App\Http\Controllers\Admin\CategoryController::class, 'create'])->name('admin.category.store');
    Route::post('/category/store', [App\Http\Controllers\Admin\CategoryController::class, 'store'])->name('admin.category.store');
    Route::get('/category/destroy/{id}', [App\Http\Controllers\Admin\CategoryController::class, 'destroy'])->name('admin.category.destroy');
    //Position
    Route::get('/position/show', [App\Http\Controllers\Admin\PositionController::class, 'show'])->name('admin.position.show');
    Route::post('/position/update', [App\Http\Controllers\Admin\PositionController::class, 'update'])->name('admin.position.update');
    Route::get('/position/edit/{id}', [App\Http\Controllers\Admin\PositionController::class, 'edit'])->name('admin.position.edit');
    Route::get('/position/store', [App\Http\Controllers\Admin\PositionController::class, 'create'])->name('admin.position.store');
    Route::post('/position/store', [App\Http\Controllers\Admin\PositionController::class, 'store'])->name('admin.position.store');
    Route::get('/position/destroy/{id}', [App\Http\Controllers\Admin\PositionController::class, 'destroy'])->name('admin.position.destroy');
});
Route::post('/subscribe', [App\Http\Controllers\UserController::class, 'subscribe'])->name('subscribe');
Route::get('/{alias}', [App\Http\Controllers\PostController::class, 'detail'])->name('post.detail');
Route::get('/tin-tuc/{alias}', [App\Http\Controllers\PostController::class, 'index'])->name('post.index');
Route::get('/cua-hang/{alias}', [App\Http\Controllers\StoreController::class, 'detail'])->name('store.detail');
Route::get('/dia-diem/{alias}', [App\Http\Controllers\PositionController::class, 'index'])->name('position.index');
Route::get('/danh-muc/{alias}', [App\Http\Controllers\CategoryController::class, 'index'])->name('category.index');
Route::any('/profile/edit', [App\Http\Controllers\UserController::class, 'profile'])->name('profile');

Route::post('/auth/login', [App\Http\Controllers\Auth\LoginController::class, 'submitLogin'])->name('auth.login');

Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');

Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');

Route::get('/auth/facebook/redirect', [App\Http\Controllers\Auth\SocialController::class, 'redirectFacebook'])->name('login-facebook');
Route::get('/auth/facebook/callback', [App\Http\Controllers\Auth\SocialController::class, 'callbackFacebook']);