$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
    }
});
function BrowseServer(div) {
    curInputId = div;
    window.KCFinder = {
        callBackMultiple: function(files) {
            window.KCFinder = null;
            for (var i = 0; i < files.length; i++)
                {
                    fileUrl = files[i];
                    var fileName = fileUrl.split('/');
                    fileName = fileName[fileName.length-1];
                    jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-input-new').removeClass('file-input-new');
                    jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-caption-name').text(fileName).attr('title',fileName);
                    if (jQuery('#'+curInputId).length && jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-image').length){
                        if (!$('#'+curInputId+'[multiple]').length) jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-frame').remove();
                        var count = jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-image').length;
                        var html = '<div class="file-preview-frame" id="preview-'+$.now()+'-'+(count)+'" data-fileindex="'+(count)+'" data-template="image"><div class="kv-file-content"> <img src="'+fileUrl+'" class="kv-preview-data file-preview-image" title="'+fileName+'" alt="'+fileName+'" style="width:auto;height:160px;"> </div><div class="file-thumbnail-footer"> <div class="file-footer-caption" title="'+fileName+'">'+fileName+' <br></div> <div class="file-actions"> <div class="file-footer-buttons"> <button type="button" class="kv-file-zoom btn btn-link btn-xs btn-icon" title="View Details"><i class="icon-zoomin3"></i></button>     </div>  <div class="file-upload-indicator" title="Not uploaded yet"><i class="icon-file-plus text-slate"></i></div> <div class="clearfix"></div> </div> </div> </div>';
                        if (jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-live-thumbs').length)
                            jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-live-thumbs').append(html);
                        else jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-initial-thumbs').append(html);
                        jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-caption-name:last').prepend('<i class="glyphicon glyphicon-file kv-caption-icon"></i>');
                        var values = [];
                        if ($('#'+curInputId+'[multiple]').length) values.push(jQuery('input[name="'+curInputId+'"]').val().replace(/^\//,''));
                        values.push(fileUrl.replace(/^\//,''));
                        jQuery('input[name="'+curInputId+'"]').val(values.filter(x => x !== '').join('|'));
                        
                    }else{
                        if (!$('#'+curInputId+'[multiple]').length) jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-frame').remove();
                        var html = '<div class="file-preview"> <div class="close fileinput-remove">Ă—</div> <div class="file-drop-disabled"> <div class="file-preview-thumbnails"> <div class="file-live-thumbs"> <div class="file-preview-frame" id="preview-'+$.now()+'-0" data-fileindex="0" data-template="image"><div class="kv-file-content"> <img src="'+fileUrl+'" class="kv-preview-data file-preview-image" title="'+fileName+'" alt="'+fileName+'" style="width:auto;height:160px;"> </div><div class="file-thumbnail-footer"> <div class="file-footer-caption" title="'+fileName+'">'+fileName+' <br></div> <div class="file-actions"> <div class="file-footer-buttons"> <button type="button" class="kv-file-zoom btn btn-link btn-xs btn-icon" title="View Details"><i class="icon-zoomin3"></i></button>     </div>  <div class="file-upload-indicator" title="Not uploaded yet"><i class="icon-file-plus text-slate"></i></div> <div class="clearfix"></div> </div> </div> </div>  </div> </div></div> <div class="clearfix"></div>    <div class="file-preview-status text-center text-success"></div> <div class="kv-fileinput-error file-error-message" style="display: none;"></div> </div> </div>';
                        jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-input').prepend(html);
                        jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-caption-name').prepend('<i class="glyphicon glyphicon-file kv-caption-icon"></i>');
                        jQuery('input[name="'+curInputId+'"]').val(fileUrl.replace(/^\//,''));
                        //jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview:last')
                    }
                    jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-thumbnails').each(function(){
                        if (!$(this).html().trim().length) $(this).parents('.file-preview').remove();
                    });
                }
        },
        callBack: function(url) {
            fileUrl = url;
            if($('#'+curInputId).data('type')==='files')
                $('[name="'+curInputId+'"]').val(url);
            //window.KCFinder = null;
            div.innerHTML = '<div style="margin:5px">Loading...</div>';
            var img = new Image();
            img.src = url;
            img.onload = function() {
                var fileName = fileUrl.split('/');
                fileName = fileName[fileName.length-1];
                jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-input-new').removeClass('file-input-new');
                jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-caption-name').text(fileName).attr('title',fileName);
                //Hien tai co anh 
                if (jQuery('#'+curInputId).length && jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-image').length){
                    if (!$('#'+curInputId+'[multiple]').length) jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-frame').remove();
                    var count = jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-image').length;
                    var html = '<div class="file-preview-frame" id="preview-'+$.now()+'-'+(count)+'" data-fileindex="'+(count)+'" data-template="image"><div class="kv-file-content"> <img src="'+fileUrl+'" class="kv-preview-data file-preview-image" title="'+fileName+'" alt="'+fileName+'" style="width:auto;height:160px;"> </div><div class="file-thumbnail-footer"> <div class="file-footer-caption" title="'+fileName+'">'+fileName+' <br></div> <div class="file-actions"> <div class="file-footer-buttons"> <button type="button" class="kv-file-zoom btn btn-link btn-xs btn-icon" title="View Details"><i class="icon-zoomin3"></i></button>     </div>  <div class="file-upload-indicator" title="Not uploaded yet"><i class="icon-file-plus text-slate"></i></div> <div class="clearfix"></div> </div> </div> </div>';
                    /*
                    if (jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-initial-thumbs').length){
                        $('.file-preview-frame').remove();
                    }*/
                    if (jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-live-thumbs').length)
                        jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-live-thumbs').append(html);
                    else jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-initial-thumbs').append(html);
                    jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-caption-name:last').prepend('<i class="glyphicon glyphicon-file kv-caption-icon"></i>');
                    var values = [];
                    if ($('#'+curInputId+'[multiple]').length) values.push(jQuery('input[name="'+curInputId+'"]').val().replace(/^\//,''));
                    values.push(fileUrl.replace(/^\//,''));
                    jQuery('input[name="'+curInputId+'"]').val(values.clean().join(','));
                    
                }else{
                    if (!$('#'+curInputId+'[multiple]').length) jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-frame').remove();
                    var html = '<div class="file-preview"> <div class="close fileinput-remove">Ă—</div> <div class="file-drop-disabled"> <div class="file-preview-thumbnails"> <div class="file-live-thumbs"> <div class="file-preview-frame" id="preview-'+$.now()+'-0" data-fileindex="0" data-template="image"><div class="kv-file-content"> <img src="'+fileUrl+'" class="kv-preview-data file-preview-image" title="'+fileName+'" alt="'+fileName+'" style="width:auto;height:160px;"> </div><div class="file-thumbnail-footer"> <div class="file-footer-caption" title="'+fileName+'">'+fileName+' <br></div> <div class="file-actions"> <div class="file-footer-buttons"> <button type="button" class="kv-file-zoom btn btn-link btn-xs btn-icon" title="View Details"><i class="icon-zoomin3"></i></button>     </div>  <div class="file-upload-indicator" title="Not uploaded yet"><i class="icon-file-plus text-slate"></i></div> <div class="clearfix"></div> </div> </div> </div>  </div> </div></div> <div class="clearfix"></div>    <div class="file-preview-status text-center text-success"></div> <div class="kv-fileinput-error file-error-message" style="display: none;"></div> </div> </div>';
                    jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-input').prepend(html);
                    jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-caption-name').prepend('<i class="glyphicon glyphicon-file kv-caption-icon"></i>');
                    jQuery('input[name="'+curInputId+'"]').val(fileUrl.replace(/^\//,''));
                    //jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview:last')
                }
                jQuery('input[name="'+curInputId+'"]').parents('.form-group').find('.file-preview-thumbnails').each(function(){
                    if (!$(this).html().trim().length) $(this).parents('.file-preview').remove();
                });
            }
            
        }
    };
    var type = $('#'+curInputId).data('type')||'images';
    window.open('/scripts/kcfinder/browse.php?type='+type,
        'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
        'directories=0, resizable=1, scrollbars=0, width=800, height=600'
    );
}
var Plugins = function () {

    var _componentInit = function() {
        $('.tokenfield-delimiter').tokenfield({
            delimiter: ', '
        });
        $('.form-input-styled').uniform();
        $('select.select').select2({
            minimumResultsForSearch: Infinity
        });
        $('.form-control-file').uniform({
            fileButtonClass: 'action btn bg-blue',
            selectClass: 'uniform-select bg-pink-400 border-pink-400'
        });
        if ($(".file-input-extensions").length) {
            var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                '  <div class="modal-content">\n' +
                '    <div class="modal-header">\n' +
                '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                '    </div>\n' +
                '    <div class="modal-body">\n' +
                '      <div class="floating-buttons btn-group"></div>\n' +
                '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>\n';
            var previewZoomButtonClasses = {
                toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
                fullscreen: 'btn btn-default btn-icon btn-xs',
                borderless: 'btn btn-default btn-icon btn-xs',
                close: 'btn btn-default btn-icon btn-xs'
            };
            // Icons inside zoom modal classes
            var previewZoomButtonIcons = {
                prev: '<i class="icon-arrow-left32"></i>',
                next: '<i class="icon-arrow-right32"></i>',
                toggleheader: '<i class="icon-menu-open"></i>',
                fullscreen: '<i class="icon-screen-full"></i>',
                borderless: '<i class="icon-alignment-unalign"></i>',
                close: '<i class="icon-cross3"></i>'
            };
        
            // File actions
            var fileActionSettings = {
                zoomClass: 'btn btn-link btn-xs btn-icon',
                zoomIcon: '<i class="icon-zoomin3"></i>',
                dragClass: 'btn btn-link btn-xs btn-icon',
                dragIcon: '<i class="icon-three-bars"></i>',
                removeClass: 'btn btn-link btn-icon btn-xs',
                removeIcon: '<i class="icon-trash"></i>',
                indicatorNew: '<i class="icon-file-plus text-slate"></i>',
                indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                indicatorError: '<i class="icon-cross2 text-danger"></i>',
                indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
            };
            $(".file-input-extensions").each(function(){
                var initPreview = $('input[name="'+this.id+'"]').val().split(',').filter(x => x !== '');
                for(i=0;i<initPreview.length;i++){
                    initPreview[i] = initPreview[i].replace(/^\//,'');
                }            
                var initPreviewConfig = [];
                for(i=0;i<initPreview.length;i++){
                    var fileNames = initPreview[i].split('/');
                    var fileName = fileNames[fileNames.length-1];
                    var obj = {caption: fileName, key: i+1, showDrag: false};
                    initPreviewConfig.push(obj);
                }
                var options = {
                    browseLabel: 'Chọn',
                    removeLabel: 'Xóa',
                    browseClass: 'btn btn-primary',
                    uploadClass: 'btn btn-default',
                    browseIcon: '<i class="icon-file-plus"></i>',
                    uploadIcon: '<i class="icon-file-upload2"></i>',
                    removeIcon: '<i class="icon-cross3"></i>',
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        modal: modalTemplate
                    },
                    maxFilesNum: 10,            
                    allowedFileExtensions: ["jpg", "gif", "png", "txt"],
                    initialCaption: 'Không có ảnh',
                    previewZoomButtonClasses: previewZoomButtonClasses,
                    previewZoomButtonIcons: previewZoomButtonIcons,
                    fileActionSettings: fileActionSettings,
                    initialPreviewAsData: true,
                    overwriteInitial: false,
                };
                if (initPreviewConfig.length) {
                    options.initialPreview = initPreview;
                    options.initialPreviewConfig = initPreviewConfig;
                }
                $(this).fileinput(options);
            }).on('filecleared', function(event) {
                $('input[name="'+event.target.id+'"]').parents('.form-group').find('.file-preview').remove();
                $('input[name="'+event.target.id+'"]').val('');
            });
        }
        // Select with search
        $('.select-search').select2();
        if ($('.ckeditor').length){
            $('.ckeditor').each(function(){
                var id = $(this).attr('id');
                if (id) {
                    CKEDITOR.replace(id);
                }
            });
        }
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentInit();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    console.log('Plugins init')
    Plugins.init();
});