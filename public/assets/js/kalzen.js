$( document ).ready(function() {
    $('.datatable-basic').DataTable();
    CKFinder.setupCKEditor();
if (document.getElementById( 'content' ))
{
    CKEDITOR.replace( 'content' );
}

var upload_avatar = document.getElementById( 'avatar' );
var upload_banner = document.getElementById( 'banner' );
var upload_image = document.getElementById( 'image' );
if (upload_avatar)
{
upload_avatar.onclick = function() {
	selectFileWithCKFinder( 'avatar', 'img-avatar', 'url-avatar');
};
}
if (upload_banner)
{
upload_banner.onclick = function() {
	selectFileWithCKFinder( 'banner', 'img-banner', 'url-banner');
};
}
if (upload_image)
{
upload_image.onclick = function() {
	selectFileWithCKFinder( 'image', 'img-image', 'url-image');
};
}
    function selectFileWithCKFinder( elementId, image, url ) {
        CKFinder.popup( {
            chooseFiles: true,
            width: 800,
            height: 600,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    var file = evt.data.files.first();
                    var output = document.getElementById( elementId );
                    var thumbnail = document.getElementById( image );
                    var path = document.getElementById( url );
                    output.value = file.getUrl();
                    thumbnail.src = file.getUrl();
                    path.innerHTML = file.getUrl();
                } );
    
                finder.on( 'file:choose:resizedImage', function( evt ) {
                    var output = document.getElementById( elementId );
                    output.value = evt.data.resizedUrl;
                } );
            }
        } );
    }
});